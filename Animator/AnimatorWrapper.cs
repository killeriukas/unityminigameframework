﻿using System;
using TMI.LogManagement;
using TMI.State;
using UnityEngine;

namespace TMI.Wrapper {

    public class AnimatorWrapper : IAnimator, ILoggable {

		private readonly Animator animator;

		public AnimatorWrapper(Animator animator) {
			Assert.IsNull(animator, "Animator cannot be null!");
			this.animator = animator;
		}

        public RuntimeAnimatorController runtimeAnimatorController {
            set {
                animator.runtimeAnimatorController = value;
            }
        }

        public bool keepAnimatorControllerStateOnDisable {
            set {
                animator.keepAnimatorControllerStateOnDisable = value;
            }
        }

        public void SetTrigger(string parameterId) {
            Logging.Log(this, "Trigger the animation [{0}].", parameterId);
			animator.SetTrigger(parameterId);
		}

        public void SetBool(string parameterId, bool value) {
            animator.SetBool(parameterId, value);
        }

        public void Play(string stateName, int layer, float normalizedTime) {
            animator.Play(stateName, layer, normalizedTime);
        }

		public TBehaviour GetBehaviour<TBehaviour>() where TBehaviour : StateMachineBehaviour {
			return animator.GetBehaviour<TBehaviour>();
		}

        public TBehaviour GetBehaviourById<TBehaviour>(string id) where TBehaviour : StateMachineBehaviourWithId {
            TBehaviour[] behaviours = animator.GetBehaviours<TBehaviour>();

            TBehaviour foundBehaviour = default(TBehaviour);

            foreach(TBehaviour behaviour in behaviours) {

                Assert.IsTrue<ArgumentException>(foundBehaviour != default(TBehaviour) && id.Equals(behaviour.id), "Cannot have more than one StateMachineBehaviourWithId with the same id [" + id + "] on the same animator!");

                if(id.Equals(behaviour.id)) {
                    foundBehaviour = behaviour;
                }
            }

            if(foundBehaviour == default(TBehaviour)) {
                throw new ArgumentException("Passed id was not found on the animator! Id: " + id);
            } else {
                return foundBehaviour;
            }
        }

        public void SetFloat(string parameterId, float value) {
            animator.SetFloat(parameterId, value);
        }
    }


}