﻿using TMI.State;
using UnityEngine;

namespace TMI.Wrapper {

	public interface IAnimator {
        void Play(string stateName, int layer, float normalizedTime);
        void SetTrigger(string parameterId);
		TBehaviour GetBehaviour<TBehaviour>() where TBehaviour : StateMachineBehaviour;
        TBehaviour GetBehaviourById<TBehaviour>(string id) where TBehaviour : StateMachineBehaviourWithId;
        void SetFloat(string parameterId, float value);
        RuntimeAnimatorController runtimeAnimatorController { set; }
        bool keepAnimatorControllerStateOnDisable { set; }
        void SetBool(string parameterId, bool value);
    }

}