﻿using UnityEngine;

namespace TMI.State {

    public abstract class StateMachineBehaviourWithId : StateMachineBehaviour {

        [SerializeField]
        private string _id;

        public string id { get { return _id; } }

    }
}