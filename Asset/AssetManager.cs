﻿using TMI.Core;
using UnityEngine;
using System.Collections;
using TMI.Operation;

namespace TMI.AssetManagement {

	public class AssetManager : BaseManager, IAssetManager {

		//May need to store running coroutines to clean after yourself later on

		public IHandle LoadAsset<TAssetType>(string resourcePath, IAsyncOperation<Object> asyncOperation) where TAssetType : Object {
			LoadHandle<Object> handle = CreateHandle(asyncOperation);
			BaseLoader<Object> loader = AssetLoader.LoadAsync<TAssetType>(resourcePath);

			StartCoroutine(RunCoroutine(loader, handle));

			return handle;
		}

		public IHandle LoadFakeAsset(System.TimeSpan duration, IAsyncOperation<Object> asyncOperation) {

			LoadHandle<Object> handle = CreateHandle(asyncOperation);
			BaseLoader<Object> loader = FakeLoader.LoadAsync(duration);

			StartCoroutine(RunCoroutine(loader, handle));
			return handle;
		}

		private LoadHandle<Object> CreateHandle(IAsyncOperation<Object> asyncOperation) {
			IAsyncOperationExecutor<Object> asyncOperationExecutor = (IAsyncOperationExecutor<Object>)asyncOperation;
			LoadHandle<Object> handle = new LoadHandle<Object>(asyncOperationExecutor);
			return handle;
		}


		public IHandle LoadAssetGroup(IAssetGroup assetGroup, IAsyncOperation<IAssetGroupComplete> asyncOperation) {

			//we can do that because we always know that async operation is actually an executor. But be careful!
			IAsyncOperationExecutor<IAssetGroupComplete> asyncOperationExecutor = (IAsyncOperationExecutor<IAssetGroupComplete>)asyncOperation;

			//we can do that because we always know that asset group interface is actually a full asset group. But be careful!
			AssetGroup fullAssetGroup = (AssetGroup)assetGroup;

			Assert.IsTrue<System.InvalidOperationException>(fullAssetGroup.isComplete, "Cannot load asset group which is already complete, or empty to begin with!");

			LoadAssetGroupHandle handle = new LoadAssetGroupHandle(asyncOperationExecutor, fullAssetGroup);

			foreach(string resourcePath in fullAssetGroup.paths) {
				System.Type resourceType = fullAssetGroup.GetResourceType(resourcePath);
				BaseLoader<Object> loader = AssetLoader.LoadAsync(resourcePath, resourceType);
				StartCoroutine(RunCoroutine(loader, handle));
			}

			return handle;
		}

		private IEnumerator RunCoroutine<TAssetType>(BaseLoader<TAssetType> loader, BaseHandle<TAssetType> handle) {

			//never load on the first frame, because it's an async operation after all
			yield return null;

			while(!loader.isDone) {
				handle.SetProgress(loader);
				yield return null;
			}

			if(handle.isCanceled) {
				loader.Unload();
			} else {
				handle.Complete(loader);
			}
		}

		public void UnloadAsset(Object prefab) {
			Resources.UnloadAsset(prefab);
		}

		protected override void OnDestroy() {
			Resources.UnloadUnusedAssets();

			base.OnDestroy();
		}
	}

}