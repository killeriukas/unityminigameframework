﻿using TMI.Core;
using TMI.Operation;
using UnityEngine;

namespace TMI.AssetManagement {

    public interface IAssetManager : IManager {

        IHandle LoadFakeAsset(System.TimeSpan duration, IAsyncOperation<Object> asyncOperation);
        IHandle LoadAsset<TAssetType>(string resourcePath, IAsyncOperation<Object> asyncOperation) where TAssetType : Object;
        IHandle LoadAssetGroup(IAssetGroup assetGroup, IAsyncOperation<IAssetGroupComplete> asyncOperation);
        void UnloadAsset(Object prefab);
    }


}

