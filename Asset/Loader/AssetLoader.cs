﻿using UnityEngine;

namespace TMI.AssetManagement {

	public class AssetLoader : BaseLoader<Object> {

		private readonly ResourceRequest resourceRequest;

		public override Object asset {
			get {
				return resourceRequest.asset;
			}
		}

		public override void Unload() {
			Resources.UnloadAsset(asset);
		}

		private AssetLoader(string path, ResourceRequest resourceRequest) : base(path, new UnityAsyncOperation(resourceRequest)) {
			this.resourceRequest = resourceRequest;
		}

        public static BaseLoader<Object> LoadAsync(string resourcePath, System.Type type) {
            ResourceRequest resourceLoader = Resources.LoadAsync(resourcePath, type);
            AssetLoader asset = new AssetLoader(resourcePath, resourceLoader);
            return asset;
        }

		public static BaseLoader<Object> LoadAsync<TObject>(string resourcePath) where TObject : Object {
			ResourceRequest resourceLoader = Resources.LoadAsync<TObject>(resourcePath);
			AssetLoader asset = new AssetLoader(resourcePath, resourceLoader);
			return asset;
		}

    }

}