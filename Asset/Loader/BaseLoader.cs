﻿using UnityEngine;

namespace TMI.AssetManagement {

	public abstract class BaseLoader<TAsset> : IAssetLoader<TAsset> {

		private readonly IAsyncOperation asyncOperation;

		public string path { get; private set; }
		public float progress => asyncOperation.progress;
		public bool isDone => asyncOperation.isDone;

		public abstract void Unload();

		public abstract TAsset asset { get; }

		protected BaseLoader(string path, IAsyncOperation asyncOperation) {
			this.path = path;
			this.asyncOperation = asyncOperation;
		}

	}

}