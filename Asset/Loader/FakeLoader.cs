﻿using TMI.LogManagement;
using UnityEngine;

namespace TMI.AssetManagement {

	public class FakeLoader : BaseLoader<Object>, ILoggable {

		public override Object asset {
			get {
				Assert.IsFalse<System.InvalidOperationException>(isDone, "Cannot receive the fake asset! It's still loading. Progress: " + progress);

				Logging.Log(this, "Trying to receive an asset from the fake loader! Will always return null, when the progress has finished!");
				return null;
			}
		}

		private FakeLoader(System.TimeSpan duration) : base("", new FakeAsyncOperation(duration)) {
			
		}

		public static FakeLoader LoadAsync(System.TimeSpan duration) {
			return new FakeLoader(duration);
		}

		public override void Unload () {
			Logging.Log(this, "Trying to unload an asset from the fake loader! Will do nothing always!");
		}
	}

}