﻿
namespace TMI.AssetManagement {

	public interface IAssetLoader<TAsset> : ILoader {
		TAsset asset { get; }
	}

}