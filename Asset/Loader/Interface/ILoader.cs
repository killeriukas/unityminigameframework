﻿
namespace TMI.AssetManagement {

	public interface ILoader {
		string path { get; }
		float progress { get; }
	}

}