﻿using System;
using TMI.TimeManagement;
using UnityEngine;

namespace TMI.AssetManagement {

	public class FakeAsyncOperation : IAsyncOperation {

		private readonly MonotonicTime endTime;
		private readonly TimeSpan duration;

		public FakeAsyncOperation(TimeSpan duration) {
			this.endTime = MonotonicTime.now + duration;
			this.duration = duration;
		}

		public float progress {
			get {
				MonotonicTime now = MonotonicTime.now;
				TimeSpan timeLeft = endTime - now;
				float durationLeft = (float)timeLeft.TotalSeconds;

				float durationSec = (float)duration.TotalSeconds;

				float p = 1f - Mathf.Clamp01(durationLeft / durationSec);
				return p;
			}
		}

		public bool isDone => progress >= 1f;

	}

}