﻿
namespace TMI.AssetManagement {

	public interface IAsyncOperation {
		
		float progress { get; }
		bool isDone { get; }

	}

}