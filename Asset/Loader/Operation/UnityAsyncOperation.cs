﻿using UnityEngine;

namespace TMI.AssetManagement {

	public class UnityAsyncOperation : IAsyncOperation {

		private readonly AsyncOperation asyncOperation;

		public UnityAsyncOperation(AsyncOperation asyncOperation) {
			this.asyncOperation = asyncOperation;
		}

		public float progress => asyncOperation.progress;

		public bool isDone => asyncOperation.isDone;

	}

}