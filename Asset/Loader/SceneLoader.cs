﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace TMI.AssetManagement {

	public class SceneLoader : BaseLoader<Scene> {

		public override Scene asset {
			get {
				Assert.IsFalse<System.InvalidOperationException>(isDone, "Cannot receive the scene! It's still loading. Progress: " + progress);

				Scene loadedScene = SceneManager.GetSceneByName(path);
				return loadedScene;
			}
		}

		private SceneLoader(string path, UnityAsyncOperation unityAsyncOperation) : base(path, unityAsyncOperation) {
			
		}

		public static SceneLoader LoadAsync(string sceneName) {
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
			UnityAsyncOperation unityAsyncOperation = new UnityAsyncOperation(asyncOperation);
			SceneLoader scene = new SceneLoader(sceneName, unityAsyncOperation);
			return scene;
		}

		public override void Unload () {
			SceneManager.UnloadSceneAsync(asset);
		}
	}

}