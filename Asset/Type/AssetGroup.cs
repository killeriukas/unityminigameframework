﻿using System.Collections.Generic;
using UnityEngine;

namespace TMI.AssetManagement {

	public class AssetGroup : BaseGroup<Object>, IAssetGroup, IAssetGroupComplete {

        private readonly Dictionary<string, System.Type> assetTypeByAssetId = new Dictionary<string, System.Type>();

        private AssetGroup() {

		}

        public System.Type GetResourceType(string resourcePath) {
            return assetTypeByAssetId[resourcePath];
        }

        public static IAssetGroup Create() {
			return new AssetGroup();
		}

		public TAsset GetAsset<TAsset>(string path) where TAsset : Object {
			Object foundAsset = Get(path);
			TAsset convertedAsset = (TAsset)foundAsset;
			return convertedAsset;
		}

        public void Add<TAssetType>(string path) where TAssetType : Object {
            bool assetAdded = TryAddingAsset(path);
            if(assetAdded) {
                System.Type assetType = typeof(TAssetType);
                assetTypeByAssetId.Add(path, assetType);
            }
        }

        public void AddGameObject(string path) {
            Add<GameObject>(path);
        }
    }

}