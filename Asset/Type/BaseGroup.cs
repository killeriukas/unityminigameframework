﻿using System.Collections.Generic;
using TMI.LogManagement;

namespace TMI.AssetManagement {

	public abstract class BaseGroup<TAsset> : IGroup, IGroupComplete<TAsset>, ILoggable {

		private float _progress = 0f;

		private readonly Dictionary<string, float> loadingPercentageByAssetId = new Dictionary<string, float>();
		private readonly Dictionary<string, TAsset> loadedAssets = new Dictionary<string, TAsset>();

		protected BaseGroup() {

		}

		public IEnumerable<string> paths {
			get { 
				return loadingPercentageByAssetId.Keys;
			}
		}

        protected bool TryAddingAsset(string path) {
            if(loadingPercentageByAssetId.ContainsKey(path)) {
                Logging.LogWarning(this, "Adding the duplicate asset for loading [{0}]. Skipping it.", path);
                return false;
            } else {
                loadingPercentageByAssetId.Add(path, 0f);
                return true;
            }
        }

		public void LoadComplete(string path, TAsset loadedAsset) {
			Assert.IsTrue<System.InvalidOperationException>(isComplete, "Cannot load more assets than requested!");
			loadingPercentageByAssetId[path] = 1f;
			loadedAssets.Add(path, loadedAsset);
		}

		public TAsset Get(string path) {
			TAsset foundAsset = loadedAssets[path];
			return foundAsset;
		}

		public IEnumerable<TAsset> GetAll() {
			return loadedAssets.Values;
		}

		public void SetProgress(string path, float progress) {
			loadingPercentageByAssetId[path] = progress;

			int assetCount = loadingPercentageByAssetId.Count;
			float totalProgress = 0f;

			foreach(var kvp in loadingPercentageByAssetId) {
				totalProgress += kvp.Value;
			}

			_progress = totalProgress / (float)assetCount;
		}

		public float progress {
			get {
				return _progress;
			}			
		}

		public bool isComplete {
			get {
				return loadingPercentageByAssetId.Count == loadedAssets.Count;
			}
		}

	}

}