﻿using UnityEngine;

namespace TMI.AssetManagement {

	public class GenericUnityAsset<TAsset> : IAsset where TAsset : Object {
		public readonly TAsset asset;

		public GenericUnityAsset(TAsset asset) {
			this.asset = asset;
		}

	}


}

