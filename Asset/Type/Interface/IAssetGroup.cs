﻿using UnityEngine;

namespace TMI.AssetManagement {

	public interface IAssetGroup : IGroup {
        void Add<TAssetType>(string path) where TAssetType : Object;
        void AddGameObject(string path);
    }

}

