﻿using UnityEngine;

namespace TMI.AssetManagement {

	public interface IAssetGroupComplete : IGroupComplete<Object> {
		TAsset GetAsset<TAsset>(string path) where TAsset : Object;
	}

}