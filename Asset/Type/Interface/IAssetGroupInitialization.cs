﻿using TMI.AssetManagement;

public interface IAssetGroupInitialization {
	void InitializeAssets(IAssetGroupComplete assetGroup);
}
