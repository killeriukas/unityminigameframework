﻿
namespace TMI.AssetManagement {

	public interface IGroupComplete<TAsset> : IAsset {
		TAsset Get(string path);
	}

}