﻿
namespace TMI.AssetManagement {

	public interface ISceneGroup : IGroup {
        void Add(string path);
    }

}

