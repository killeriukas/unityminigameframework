﻿using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace TMI.AssetManagement {

	public interface ISceneGroupComplete : IGroupComplete<Scene> {
		IEnumerable<Scene> GetAll ();
	}

}