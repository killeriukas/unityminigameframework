﻿using TMI.AssetManagement;
using UnityEngine.SceneManagement;

namespace TMI.AssetManagement {

	public class SceneGroup : BaseGroup<Scene>, ISceneGroup, ISceneGroupComplete {

		private SceneGroup() {

		}

        public void Add(string path) {
            TryAddingAsset(path);
        }

        public static ISceneGroup Create() {
			return new SceneGroup();
		}

	}

}