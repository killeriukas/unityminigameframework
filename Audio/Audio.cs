﻿using Newtonsoft.Json;
using System;
using TMI.Save;
using UnityEngine;

namespace TMI.AudioManagement {

    [JsonObject(MemberSerialization.OptIn)]
    public class Audio : BaseModelGeneric<Audio.Data>, IAudio {

        [Serializable]
        public class Data : IModel {
            public float sfxVolume;
            public float musicVolume;
        }

        public Audio() {
            model.sfxVolume = 1f;
            model.musicVolume = 1f;
        }

        public bool isSfxOn { get { return model.sfxVolume > 0f; } }
        public bool isMusicOn { get { return model.musicVolume > 0f; } }

        public void SetSfxVolume(float volume) {
            model.sfxVolume = Mathf.Clamp01(volume);
        }

        public void SetMusicVolume(float volume) {
            model.musicVolume = Mathf.Clamp01(volume);
        }
    }
}