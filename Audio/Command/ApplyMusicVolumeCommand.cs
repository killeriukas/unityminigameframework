﻿using TMI.Command;
using TMI.Core;
using TMI.SettingsManagement;

namespace TMI.AudioManagement {

    public class ApplyMusicVolumeCommand : BaseCommand<Settings> {

        private float volume;

        public ApplyMusicVolumeCommand(IInitializer initializer, Settings settings) : base(initializer, settings) {
        }

        public void Setup(float volume) {
            this.volume = volume;
        }

        public override void Execute() {
            //profile.ApplyMusicVolume(volume);
            //profile.Save(serializer);
        }

    }


}