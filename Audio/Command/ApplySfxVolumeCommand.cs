﻿using TMI.Command;
using TMI.Core;
using TMI.SettingsManagement;

namespace TMI.AudioManagement {

    public class ApplySfxVolumeCommand : BaseCommand<Settings> {

        private float volume;

        public ApplySfxVolumeCommand(IInitializer initializer, Settings settings) : base(initializer, settings) {
        }

        public void Setup(float volume) {
            this.volume = volume;
        }

        public override void Execute() {
            //profile.Audio(volume);
            //profile.Save(serializer);
        }

    }


}