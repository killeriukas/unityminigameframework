﻿
namespace TMI.AudioManagement {

    public interface IAudio {
        bool isSfxOn { get; }
        bool isMusicOn { get; }
    }

}