﻿using TMI.Core;
using TMI.ProfileManagement;
using TMI.Save;

namespace TMI.Command {

	public abstract class BaseCommand<TProfile> where TProfile : IProfile  {

        protected readonly IInitializer initializer;
		protected readonly TProfile profile;
		protected readonly ISerializer serializer;

		protected BaseCommand(IInitializer initializer, TProfile profile) {
            this.initializer = initializer;
			this.profile = profile;
			this.serializer = initializer.GetManager<ISerializer>();
		}

		public abstract void Execute();
	}

}