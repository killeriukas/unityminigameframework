﻿using System;
using System.Collections.Generic;
using TMI.Helper;

namespace TMI.ConfigManagement {

    public static class ConfigManager {

        private static FileStorageConfig fileStorageConfig = new FileStorageConfig();

        private static Dictionary<Type, BaseConfig> configDictionary = new Dictionary<Type, BaseConfig>();

        public static void Initialize() {
            
        }

        public static void AddConfig<TConfig>() where TConfig : BaseConfig {
            Type configType = typeof(TConfig);
            TConfig newConfig = GeneralHelper.CreateInstance<TConfig>(fileStorageConfig);
            configDictionary.Add(configType, newConfig);
        }

        public static TConfig GetConfig<TConfig>() where TConfig : BaseConfig {
            Type configType = typeof(TConfig);
            return (TConfig)configDictionary[configType];
        }

        public static void Clear() {
            configDictionary.Clear();
        }

    }


}