﻿using UnityEditor;
using UnityEngine;

namespace TMI.ConfigManagement {

    public class ConfigEditorWindow : EditorWindow {

        [MenuItem("Window/TMI/Config", false, 0)]
        public static void ShowWindow() {
            GetWindow<ConfigEditorWindow>("Config", true);
        }

        private FileStorageConfig fileStorageConfig;
        private FileStorageConfig.Editor fileStorageEditor;
        private LoggingConfig.Editor loggingConfigEditor;


        private void Awake() {
            fileStorageConfig = new FileStorageConfig();
            fileStorageEditor = new FileStorageConfig.Editor(fileStorageConfig);
            loggingConfigEditor = new LoggingConfig.Editor(new LoggingConfig(fileStorageConfig));
        }

        //private enum State {
        //    Uninitialized,
        //    Initialized
        //}

        //private State state = State.Uninitialized;


        
        //private LoggingConfig loggingConfig;
        //private LoggingConfig.Editor editor;

        //private Vector2 scrollBarPosition;
        //private bool showSaveButton = false;

        private void OnGUI() {
            fileStorageEditor.Render();
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            loggingConfigEditor.Render();

            //    EditorGUILayout.LabelField("Logging");

            //switch(state) {
            //    case State.Uninitialized:
            //        fileStorageConfig = new FileStorageConfig();
            //        fileStorageConfig.Initialize();

            //        loggingConfig = new LoggingConfig();
            //        loggingConfig.Initialize(fileStorageConfig);

            //        editor = new LoggingConfig.Editor(loggingConfig);
            //        state = State.Initialized;
            //        break;
            //    case State.Initialized:
            //        if(GUILayout.Button("Save Remote")) {
            //            editor.SaveRemote();
            //        }
            //        //if(GUILayout.Button("Refresh FileStorageConfig()")) {
            //        //    FileStorageConfig.Editor fileStorageConfigEditor = new FileStorageConfig.Editor(fileStorageConfig);
            //        //    //fileStorageConfigEditor.
            //        //}

            //        //     EditorGUILayout.HelpBox("Save changes or lose work", MessageType.Warning);

            //        scrollBarPosition = EditorGUILayout.BeginScrollView(scrollBarPosition, false, true);

            //        List<string> test = new List<string>(editor.configKeys);
            //        foreach(string keyName in test) {
            //            bool currentValue;
            //            bool hasFound = loggingConfig.TryGetValue(keyName, out currentValue);
            //            if(hasFound) {
            //                bool newValue = EditorGUILayout.ToggleLeft(keyName, currentValue);
            //                editor.SetValue(keyName, newValue);
            //            }
            //        }

            //        EditorGUILayout.EndScrollView();
            //        break;
            //}
        }

    }


}