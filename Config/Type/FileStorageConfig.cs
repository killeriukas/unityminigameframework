﻿using System;
using System.Collections.Generic;
using System.IO;
using TMI.Helper;
using TMI.LogManagement;
using TMI.Wrapper;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TMI.ConfigManagement {

    public class FileStorageConfig : IConfig, ILoggable {

        private const string fileStorageConfigFileName = "FileStorageConfig";
        private readonly string absoluteConfigDirectory;
        private readonly string fileStorageConfigFilePath;

        private Dictionary<string, string> fileLocationByName;

        public FileStorageConfig() {
            absoluteConfigDirectory = Path.Combine(Application.dataPath, "Config/Resources");
            fileStorageConfigFilePath = Path.Combine(absoluteConfigDirectory, fileStorageConfigFileName + ".json");

            Logging.Log(this, "Initializing FileStorageConfig.");
            if(FileHelper.ExistsBasedOnPlatform(fileStorageConfigFilePath)) {
                Logging.Log(this, "File found. Loading.");
                TextAsset textAsset = Resources.Load<TextAsset>(fileStorageConfigFileName);
                string configContent = textAsset.text;
                Logging.Log(this, "File loaded with content [{0}].", configContent);
                fileLocationByName = SerializationHelper.Deserialize<Dictionary<string, string>>(configContent);
            } else {
                CreateDefaultConfigFile();
            }
        }

        public string GetDefaultFilePath<TClass>() where TClass : IConfig {
            string className = GetConfigClassName<TClass>();
            string absolutePath = Path.Combine(absoluteConfigDirectory, className + ".json");
            return absolutePath;
        }

        public string GetFilePath<TClass>() where TClass : IConfig {
            string className = GetConfigClassName<TClass>();
            string relativeConfigPath = fileLocationByName[className];
            string absolutePath = Path.Combine(ConstantHelper.persistentDataPath, relativeConfigPath);
            return absolutePath;
        }

        private string GetConfigClassName<TClass>() where TClass : IConfig {
            return typeof(TClass).Name;
        }

        private void CreateDefaultConfigFile() {
#if UNITY_EDITOR
            Logging.Log(this, "Creating a new config file at [{0}].", fileStorageConfigFilePath);
            if(!Directory.Exists(absoluteConfigDirectory)) {
                Logging.Log(this, "Creating a directory [{0}].", absoluteConfigDirectory);
                Directory.CreateDirectory(absoluteConfigDirectory);
                Logging.Log(this, "Directory created.", absoluteConfigDirectory);
            }

            fileLocationByName = new Dictionary<string, string>();

            Logging.Log(this, "Populating the default file location list.");
            List<Type> foundTypes = ReflectionHelper.FindAllAssignableClassTypesForClass<BaseConfig>();
            foreach(Type type in foundTypes) {
                string className = type.Name;
                string filePath = "Config/" + className + ".json"; //cannot do Path.Combine(), cuz some android apps crash // Path.Combine("Config", className + ".json");
                fileLocationByName.Add(className, filePath);
            }

            Logging.Log(this, "Population done. Ready to serialize.");
            string fileConfigContent = SerializationHelper.Serialize(fileLocationByName);
            Logging.Log(this, "File serialized with content [{0}]. Writing to a file [{1}].", fileConfigContent, fileStorageConfigFilePath);
            File.WriteAllText(fileStorageConfigFilePath, fileConfigContent);
            AssetDatabase.Refresh();
            Logging.Log(this, "Default config file created.");
#else
            throw new System.InvalidOperationException("Cannot CreateDefaultConfigFile() for FileStorageConfig from outside the editor!");
#endif
        }

#if UNITY_EDITOR
        public class Editor : EditorWrapper<FileStorageConfig> {

            private bool isMenuFoldout = false;

            public Editor(FileStorageConfig _class) : base(_class) {
            }

            public void Render() {
                isMenuFoldout = EditorGUILayout.Foldout(isMenuFoldout, "Config files available");
                if(isMenuFoldout) {
                    ++EditorGUI.indentLevel;
                    foreach(var kvp in _class.fileLocationByName) {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(kvp.Key);
                        EditorGUILayout.LabelField(kvp.Value);
                        EditorGUILayout.EndHorizontal();
                    }
                    --EditorGUI.indentLevel;
                }
            }

        }
#endif

    }

}