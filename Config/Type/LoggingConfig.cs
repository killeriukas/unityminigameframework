﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMI.Helper;
using TMI.LogManagement;
using TMI.Wrapper;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace TMI.ConfigManagement {

    public class LoggingConfig : BaseConfig {

        private string remoteFilePath;
        private string defaultConfigPath;

        private Dictionary<string, bool> loggingConfig;

        public LoggingConfig(FileStorageConfig fileStorageConfig) {
            Logging.Log(this, "Logging config started initialization.");
            remoteFilePath = fileStorageConfig.GetFilePath<LoggingConfig>();
            Logging.Log(this, "Remote file path [{0}].", remoteFilePath);

            string directory = Path.GetDirectoryName(remoteFilePath);
            Logging.Log(this, "Remote directory path [{0}].", directory);
            if(!Directory.Exists(directory)) {
                Logging.Log(this, "Directory doesn't exist. Creating...");
                Directory.CreateDirectory(directory);
                Logging.Log(this, "Directory created at [{0}].", directory);
            }

            defaultConfigPath = fileStorageConfig.GetDefaultFilePath<LoggingConfig>();
            Logging.Log(this, "Default config path [{0}].", defaultConfigPath);
            if(FileHelper.ExistsBasedOnPlatform(defaultConfigPath)) {
                CloneDefaultFileToRemote(defaultConfigPath, remoteFilePath);
            } else {
#if UNITY_EDITOR
                CreateDefaultConfigFile(defaultConfigPath);
                AssetDatabase.Refresh();
                CloneDefaultFileToRemote(defaultConfigPath, remoteFilePath);
#else
                throw new System.InvalidOperationException("Cannot create a default config file outside of Unity.");
#endif
            }

            Logging.Log(this, "Loading remote file [{0}].", remoteFilePath);
            string content = File.ReadAllText(remoteFilePath);
            Logging.Log(this, "File loaded with content [{0}].", content);
            loggingConfig = SerializationHelper.Deserialize<Dictionary<string, bool>>(content);
            Logging.Log(this, "LoggingConfig initialization finished.");
        }

        private void CloneDefaultFileToRemote(string defaultConfigPath, string remoteConfigPath) {
            Logging.Log(this, "Cloning default file from [{0}] to [{1}].", defaultConfigPath, remoteFilePath);
            //if default file exists - just clone it to the remote destination
            if(!File.Exists(remoteConfigPath)) {
                Logging.Log(this, "Remote file doesn't exist.");
                string defaultConfigFileName = Path.GetFileNameWithoutExtension(defaultConfigPath);
                Logging.Log(this, "Default config file name [{0}].", defaultConfigFileName);
                TextAsset textAsset = Resources.Load<TextAsset>(defaultConfigFileName);
                string textAssetContent = textAsset.text;
                Logging.Log(this, "Loaded default config text asset with content [{0}]. Writing to remote config file [{1}].", textAssetContent, remoteConfigPath);
                File.WriteAllText(remoteConfigPath, textAssetContent);
            }
            Logging.Log(this, "Cloning finished.");
        }

#if UNITY_EDITOR
        private void CreateDefaultConfigFile(string filePath) {
            List<Type> foundTypes = ReflectionHelper.FindAllAssignableClassTypesForClass<ILoggable>();

            List<Type> staticClassTypes = ReflectionHelper.FindAllClassesWithAttribute<LoggableAttribute>();

            foundTypes.AddRange(staticClassTypes);

            List<string> sortedTypeNameList = foundTypes.ConvertAll(x => x.FullName);

            //sort alphabetically
            sortedTypeNameList.Sort();

            Dictionary<string, bool> config = sortedTypeNameList.ToDictionary(x => x, x => false);

            string content = SerializationHelper.Serialize(config);
            File.WriteAllText(filePath, content);
        }

        private void SaveRemote() {
            string content = SerializationHelper.Serialize(loggingConfig);
            File.WriteAllText(remoteFilePath, content);
        }

        private void OverwriteRemoteOntoDefault() {
            SaveRemote();

            string content = SerializationHelper.Serialize(loggingConfig);
            File.WriteAllText(defaultConfigPath, content);

            AssetDatabase.Refresh();
        }
#endif

        public bool TryGetValue(string key, out bool value) {
            return loggingConfig.TryGetValue(key, out value);
        }

#if UNITY_EDITOR
        public class Editor : EditorWrapper<LoggingConfig> {

            private Vector2 scrollViewPosition;
            private bool isMenuFoldout = false;

            public Editor(LoggingConfig _class) : base(_class) {
            }

            public void Render() {
                isMenuFoldout = EditorGUILayout.Foldout(isMenuFoldout, "Logging");
                if(isMenuFoldout) {
                    scrollViewPosition = EditorGUILayout.BeginScrollView(scrollViewPosition, false, true);
                    ++EditorGUI.indentLevel;
                    List<string> fileNames = new List<string>(_class.loggingConfig.Keys);

                    EditorGUILayout.BeginHorizontal();
                    if(GUILayout.Button("Select All")) {
                        foreach(string fileName in fileNames) {
                            _class.loggingConfig[fileName] = true;
                        }
                    }

                    if(GUILayout.Button("Select None")) {
                        foreach(string fileName in fileNames) {
                            _class.loggingConfig[fileName] = false;
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    foreach(string fileName in fileNames) {
                        _class.loggingConfig[fileName] = EditorGUILayout.ToggleLeft(fileName, _class.loggingConfig[fileName]);
                    }

                    EditorGUILayout.BeginHorizontal();
                    if(GUILayout.Button("Save Remote")) {
                        _class.SaveRemote();
                    }

                    if(GUILayout.Button("Overwrite Default")) {
                        _class.OverwriteRemoteOntoDefault();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndScrollView();
                    --EditorGUI.indentLevel;
                }
            }

        }
#endif

    }



}