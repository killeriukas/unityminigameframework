﻿using TMI.Notification;
using TMI.SceneManagement;
using TMI.UI;

namespace TMI.Core {

    public abstract class BaseMainMiniInitializer : BaseInitializer {

        private IExecutionManager executionManager;

        protected INotificationManager notificationManager { get; private set; }
        protected ISceneManager sceneManager { get; private set; }
        protected IUIManager uiManager { get; private set; }
        protected IPauseManager pauseManager { get; private set; }

        protected override void RegisterManagers(IAcquirer acquirer) {
            executionManager = acquirer.AcquireManager<ExecutionManager, IExecutionManager>();

            notificationManager = acquirer.AcquireManager<NotificationManager, INotificationManager>();
            sceneManager = acquirer.AcquireManager<SceneManager, ISceneManager>();
            uiManager = acquirer.AcquireManager<UIManager, IUIManager>();
            pauseManager = acquirer.AcquireManager<PauseManager, IPauseManager>();
        }

    }

}