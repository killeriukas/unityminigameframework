﻿using System;

namespace TMI.Core {

    public interface IInitializer {
        TManagerInterface GetManager<TManager, TManagerInterface>() where TManager : BaseManager, TManagerInterface, new() where TManagerInterface : class, IManager;
        TManagerInterface GetManager<TManagerInterface>() where TManagerInterface : class, IManager;
    }

}