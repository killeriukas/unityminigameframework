﻿using System;
using System.Collections.Generic;
using TMI.Helper;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.Core {

    public class CoreManager : MonoBehaviour {

        private class ManagerAcquirer : IAcquirer {

            private CoreManager coreManager;

            public ManagerAcquirer(CoreManager coreManager) {
                this.coreManager = coreManager;
            }

            public TManager AcquireManager<TManager>() where TManager : BaseManager {
                return coreManager.AcquireManager<TManager>();
            }

            public TManagerInterface AcquireManager<TManager, TManagerInterface>() where TManager : BaseManager, TManagerInterface where TManagerInterface : IManager {
                return coreManager.AcquireManager<TManager>();
            }
        }

        private static bool created = false;
        private static readonly string coreName = typeof(CoreManager).Name;


        private IAcquirer acquirer = null;
        private Dictionary<Type, IManager> managerByType = new Dictionary<Type, IManager>();

        private Dictionary<Type, bool> managerStateByType = new Dictionary<Type, bool>();
        private HashSet<Type> managerRecycleBin;
        private EventSystem uiEventSystem;
        public Camera uiCamera { get; private set; }

        private void Awake() {
            uiEventSystem = GameObject.FindObjectOfType<EventSystem>();
            Assert.IsNull(uiEventSystem, "The very first scene has to have EventSystem in it, cuz all the others will be deleted!");

            uiEventSystem.transform.SetParent(transform, false);

            GameObject foundCamera = GameObject.FindGameObjectWithTag("UICamera");

            //create default UI camera
            if(foundCamera == null) {
                foundCamera = new GameObject("UI Camera");
                foundCamera.tag = "UICamera";
                uiCamera = foundCamera.AddComponent<Camera>();

                uiCamera.clearFlags = CameraClearFlags.Depth;
                uiCamera.cullingMask = 1 << LayerMask.NameToLayer("UI");
                uiCamera.orthographic = true;
                uiCamera.orthographicSize = 8f;
                uiCamera.nearClipPlane = 0.3f;
                uiCamera.farClipPlane = 110f;
                uiCamera.depth = 0f;
                uiCamera.useOcclusionCulling = false;
            } else {
                uiCamera = foundCamera.GetComponent<Camera>();
            }           

            uiCamera.transform.SetParent(transform, false);
            created = true;
        }

        public static CoreManager Create() {
            if(created) {
                CoreManager coreManager = GameObject.FindObjectOfType<CoreManager>();
                return coreManager;
            } else {
                GameObject go = new GameObject(coreName);
                CoreManager coreManager = go.AddComponent<CoreManager>();
                return coreManager;
            }
        }

        public IAcquirer StartAcquire() {
            Assert.IsNotNull(acquirer, "Cannot acquire more than one acquirer! Have you forgot to EndAcquire()?");

            managerRecycleBin = new HashSet<Type>(managerByType.Keys);

            acquirer = new ManagerAcquirer(this);
            return acquirer;
        }

        public void EndAcquire(IAcquirer acquirer, IInitializer initializer) {
            Assert.IsFalse<ArgumentException>(this.acquirer == acquirer, "Returning the wrong acquirer!");
            this.acquirer = null;

            //delete all leftovers
            foreach(Type type in managerRecycleBin) {
                IManager manager = managerByType[type];
                managerByType.Remove(type);
                manager.Dispose();
            }
            managerRecycleBin.Clear();

            //initialize new managers
            foreach(Type type in managerByType.Keys) {
                bool isNewlyCreated = managerStateByType[type];
                managerByType[type].Setup(initializer, isNewlyCreated);
            }
            managerStateByType.Clear();
        }

        private TManager AcquireManager<TManager>() where TManager : BaseManager {
            Type managerType = typeof(TManager);

            if(managerByType.ContainsKey(managerType)) {
                managerRecycleBin.Remove(managerType);
                TManager manager = GetManager<TManager>();
                managerStateByType.Add(managerType, false);
                return manager;
            } else {
                managerStateByType.Add(managerType, true);
                GameObject newManagerGo = new GameObject(managerType.Name);
                newManagerGo.transform.SetParent(base.transform, false);

                TManager newManager = newManagerGo.AddComponent<TManager>();

                managerByType.Add(managerType, newManager);

                return newManager;
            }
        }

        public TManagerInterface GetManager<TManager, TManagerInterface>() where TManager : BaseManager, TManagerInterface, new() where TManagerInterface : class, IManager {
            Type managerType = typeof(TManager);
            TManager manager = (TManager)managerByType[managerType];
            return manager;
        }

        public TManagerInterface GetManager<TManagerInterface>() where TManagerInterface : class, IManager {
            //throw exception if it's not an interface later...
            //typeof(T).IsInterface

            List<Type> allAssignableTypes = ReflectionHelper.FindAllAssignableClassTypesForClass<TManagerInterface>();

            Type interfaceType = typeof(TManagerInterface);
            Assert.IsTrue<InvalidOperationException>(allAssignableTypes.Count == 0, "No assignable types for manager interface [" + interfaceType.Name + "] found!");
            Assert.IsTrue<InvalidOperationException>(allAssignableTypes.Count > 1, "Manager interface [" + interfaceType.Name + "] cannot have more than one class! Has: " + allAssignableTypes.Count);

            Type foundInterfaceClassType = allAssignableTypes[0];
            TManagerInterface manager = (TManagerInterface)managerByType[foundInterfaceClassType];
            return manager;
        }

        private void OnDestroy() {
            created = false;
        }

    }
}