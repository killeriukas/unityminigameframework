﻿using System;

namespace TMI.Core {

    public interface IAcquirer {
        [Obsolete("Use AcquireManager<TManager, TManagerInterface>() instead. This will be removed later on.")]
        TManager AcquireManager<TManager>() where TManager : BaseManager;
        TManagerInterface AcquireManager<TManager, TManagerInterface>() where TManager : BaseManager, TManagerInterface where TManagerInterface : IManager;
    }

}