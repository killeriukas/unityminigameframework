﻿using System;

namespace TMI.Core {

	public interface IManager : IDisposable {
		void Setup(IInitializer initializer, bool isNew);
	}

}


