﻿using System;
using TMI.Helper;
using TMI.Mathematics;

public struct Damage : IDamage {

    public double damage;
    public Chance criticalChance;
    public Percent criticalDamage;

    public Damage(double damage, Chance criticalChance, Percent criticalDamage) {
        this.damage = damage;
        this.criticalChance = criticalChance;
        this.criticalDamage = criticalDamage;
    }

    public Damage(double damage) : this(damage, Chance.zero, Percent.zero) {

    }

    public double value {
        get {

            bool hasCrit = RandomHelper.IsTrue(criticalChance);
            if(hasCrit) {
                damage = damage + criticalDamage.Apply(damage);
            }
            
            //round damage nicely, so it would end up being an integer (but in double form just for the 64bit)
            damage = Math.Round(damage);

            return damage;
        }
    }

    public static Damage operator +(Damage d1, Damage d2) {
        d1.damage += d2.damage;
        d1.criticalChance += d2.criticalChance;
        d1.criticalDamage += d2.criticalDamage;
        return d1;
    }

}