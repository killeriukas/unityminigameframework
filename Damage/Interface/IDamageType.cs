﻿
public interface IDamageType {
    Damage damage { get; }
}
