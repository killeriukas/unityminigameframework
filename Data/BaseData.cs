﻿using Newtonsoft.Json;

namespace TMI.Data {

	[JsonObject(MemberSerialization.OptIn)]
    public abstract class BaseData : IData {

		[JsonProperty("Id")]
        public string id { get; private set; }
    }
        
}