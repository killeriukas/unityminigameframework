﻿using System.Collections.Generic;
using System;
using TMI.Core;

namespace TMI.Data {

	public abstract class BaseDataManager : BaseManager, IDataManager {

        private readonly Dictionary<string, IData> dataById = new Dictionary<string, IData>();
		private readonly Dictionary<Type, List<string>> dataIdsByType = new Dictionary<Type, List<string>>();

        protected override void Awake() {
			base.Awake();
			PopulateDataById(dataById);
			PopulateDataByType(dataIdsByType);
		}

		protected abstract void PopulateDataById(Dictionary<string, IData> dataById);
		protected abstract void PopulateDataByType(Dictionary<Type, List<string>> dataIdsByType);

		public IEnumerable<string> GetAllDataIds<TData>() where TData : IData {
			Type dataType = typeof(TData);
			IEnumerable<string> ids = dataIdsByType[dataType];
			return ids;
		}

        public int GetAllDataIdsCount<TData>() where TData : IData {
            Type dataType = typeof(TData);
            List<string> ids = dataIdsByType[dataType];
            return ids.Count;
        }

        public TData GetDataById<TData>(string id) where TData : IData {
            TData foundData = (TData)dataById [id];
            return foundData;
        }

        public bool ContainsDataById(string id) {
            return dataById.ContainsKey(id);
        }

		public bool TryGetData<TData>(string id, out TData data) where TData: IData {
			IData foundValue;
			bool found = dataById.TryGetValue(id, out foundValue);
			if(found) {
				data = (TData)foundValue;
				return true;
			} else {
				data = default(TData);
				return false;
			}
		}
    }

}