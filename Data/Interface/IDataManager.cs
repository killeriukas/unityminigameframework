﻿using System.Collections.Generic;
using TMI.Core;

namespace TMI.Data {

    public interface IDataManager : IManager {
        bool TryGetData<TData>(string id, out TData data) where TData : IData;
        bool ContainsDataById(string id);
        TData GetDataById<TData>(string id) where TData : IData;
        int GetAllDataIdsCount<TData>() where TData : IData;
        IEnumerable<string> GetAllDataIds<TData>() where TData : IData;
    }

}