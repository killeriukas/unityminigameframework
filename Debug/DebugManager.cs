﻿using TMI.Core;
using TMI.UI;
using UnityEngine;

namespace TMI.DebugManagement {

    public abstract class DebugManager : BaseManager {

        public static bool debugEnabled = false;

        protected IUIManager uiManager { get; private set; }

        public override void Setup(IInitializer initializer, bool isNew) {
            base.Setup(initializer, isNew);
            uiManager = initializer.GetManager<UIManager, IUIManager>();

            if(isNew) {
                if(debugEnabled) {
                    RegisterUpdate();
                }
            }

        }

        protected override ExecutionManager.Result OnUpdate() {

            if(Input.GetKeyDown(KeyCode.P) || Input.touchCount > 3) {
                DebugMenuUIController debugMenuUIController = uiManager.LoadUI<DebugMenuUIController>();
                debugMenuUIController.Show();
            }

            return ExecutionManager.Result.Continue;
        }

        protected override void OnDestroy() {
            if(debugEnabled) {
                UnregisterUpdate();
            }
            
            base.OnDestroy();
        }

    }

}