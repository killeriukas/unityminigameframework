﻿using System;
using System.Collections.Generic;
using TMI.UI;
using TMI.UI.Extension;
using TMI.UI.Popup;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.DebugManagement {

    sealed public class DebugMenuUIController : BaseClosableUIController {

        [SerializeField]
        private UIList itemList;

        private Dictionary<string, Cell> cellById = new Dictionary<string, Cell>();

        public void AddButton(string buttonId, string buttonName, Action<PointerEventData> onClick) {
            Cell cell = itemList.CloneCell();

            UIText buttonText = cell.FindComponent<UIText>("text_debug_button");
            buttonText.text = buttonName;

            UIButton button = cell.FindComponent<UIButton>("button_debug");
            button.onButtonClick += onClick;

            cellById.Add(buttonId, cell);
        }

        public void RemoveButton(string buttonId) {
            Cell cell = cellById[buttonId];
            cellById.Remove(buttonId);
            itemList.DestroyCell(cell);
        }

        protected override void OnDestroy() {
            foreach(var kvp in cellById) {
                itemList.DestroyCell(kvp.Value);
            }
            cellById.Clear();

            base.OnDestroy();
        }

    }

}