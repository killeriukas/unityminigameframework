﻿using System.IO;
using UnityEngine;

namespace TMI.Helper {

    public static class ConstantHelper {

        public static string persistentDataPath {
            get {
#if UNITY_EDITOR
                string folderName = Application.identifier;
    #if UNITY_EDITOR_WIN
                    if(dDriveExists) {
                        return "D:/" + folderName;
                    } else {
                        return "C:/" + folderName;
                    }
    #else
                return folderName;
    #endif
#else
            return Application.persistentDataPath;
#endif
            }
        }

#if UNITY_EDITOR
        private static bool dDriveExists {
            get {
                return Directory.Exists("D:/");
            }
        }
#endif


    }


}