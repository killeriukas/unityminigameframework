﻿using UnityEditor;
using UnityEngine;

namespace TMI.Helper {

    public static class PlayerPrefsHelperEditor {

        [MenuItem("Window/TMI/PlayerPrefs/Delete", false, 0)]
        public static void DeletePlayerPrefs() {
            PlayerPrefs.DeleteAll();
        }

    }


}