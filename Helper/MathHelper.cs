﻿using System;

namespace TMI.Helper {

    public static class MathHelper {

        public static TimeSpan Clamp(TimeSpan clampValue, TimeSpan min, TimeSpan max) {
            double currentValueClamped = Clamp(clampValue.TotalSeconds, min.TotalSeconds, max.TotalSeconds);
            return TimeSpan.FromSeconds(currentValueClamped);
        }

        public static double Clamp(double clampValue, double min, double max) {
            if(clampValue < min) {
                return min;
            }

            if(clampValue > max) {
                return max;
            }

            return clampValue;
        }

        public static float Clamp(float clampValue, float min, float max) {
            if(clampValue < min) {
                return min;
            }

            if(clampValue > max) {
                return max;
            }

            return clampValue;
        }

        public static double Lerp(double a, double b, float value) {
            value = Clamp(value, 0, 1);
            return (1 - value) * a + value * b;
        }

    }

}