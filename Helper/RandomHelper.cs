﻿using TMI.Mathematics;
using UnityEngine;

namespace TMI.Helper {

	public static class RandomHelper {

		public static bool IsTrue(Chance chance) {
            //if chance is approx zero, that means return false instantly
            if(Mathf.Approximately(Chance.zero.ToFloat(), chance.ToFloat())) {
                return false;
            } else if(Mathf.Approximately(Chance.hundred.ToFloat(), chance.ToFloat())) { //if it's approx 100%, return true instantly
                return true;
            } else {
                Chance randomChance = new Chance(Random.value);
                bool hasSucceeded = chance > randomChance;
                return hasSucceeded;
            }
		}


	}


}