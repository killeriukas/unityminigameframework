﻿namespace TMI.LogManagement {

    public interface ILogOutput {
        void Log(string categoryType, string message);
        void Log(ILoggable loggable, string message);
        void LogWarning(string categoryType, string message);
        void LogWarning(ILoggable loggable, string message);
        void LogError(ILoggable loggable, string message);
    }

}
