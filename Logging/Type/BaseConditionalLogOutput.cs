﻿using System;
using TMI.ConfigManagement;
using UnityEngine;

namespace TMI.LogManagement {

    public abstract class BaseConditionalLogOutput : ILogOutput {

        private LoggingConfig loggingConfig;

        protected BaseConditionalLogOutput(LoggingConfig loggingConfig) {
            this.loggingConfig = loggingConfig;
        }

        private bool ShouldLogMessage(string typeFullName) {
            bool shouldPrint;
            bool hasFoundValue = loggingConfig.TryGetValue(typeFullName, out shouldPrint);

            if(!hasFoundValue) {
                Debug.LogWarning("It's time to rebuild LoggingConfig(). Missing file: " + typeFullName);
            }

            return shouldPrint;
        }

        private bool ShouldLogMessage(ILoggable loggable) {
            Type type = loggable.GetType();
            string typeFullName = type.FullName;
            return ShouldLogMessage(typeFullName);
        }

        public void Log(string categoryType, string message) {
            if(ShouldLogMessage(categoryType)) {
                InternalLog(categoryType, message);
            }
        }

        public void Log(ILoggable loggable, string message) {
            if(ShouldLogMessage(loggable)) {
                InternalLog(loggable, message);
            }
        }

        public void LogWarning(string categoryType, string message) {
            if(ShouldLogMessage(categoryType)) {
                InternalLogWarning(categoryType, message);
            }
        }

        public void LogWarning(ILoggable loggable, string message) {
            if(ShouldLogMessage(loggable)) {
                InternalLogWarning(loggable, message);
            }
        }

        public void LogError(ILoggable loggable, string message) {
            if(ShouldLogMessage(loggable)) {
                InternalLogError(loggable, message);
            }
        }

        protected abstract void InternalLog(string categoryType, string message);
        protected abstract void InternalLog(ILoggable loggable, string message);
        protected abstract void InternalLogWarning(ILoggable loggable, string message);
        protected abstract void InternalLogWarning(string categoryType, string message);
        protected abstract void InternalLogError(ILoggable loggable, string message);

    }

}