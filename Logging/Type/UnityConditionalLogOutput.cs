﻿using TMI.ConfigManagement;
using UnityEngine;

namespace TMI.LogManagement {

    public class UnityConditionalLogOutput : BaseConditionalLogOutput {

        public UnityConditionalLogOutput(LoggingConfig loggingConfig) : base(loggingConfig) {
        }

        protected override void InternalLog(string categoryType, string message) {
            Debug.Log(message);
        }

        protected override void InternalLog(ILoggable loggable, string message) {
            Debug.Log(message);
        }

        protected override void InternalLogWarning(string categoryType, string message) {
            Debug.LogWarning(message);
        }

        protected override void InternalLogWarning(ILoggable loggable, string message) {
            Debug.LogWarning(message);
        }

        protected override void InternalLogError(ILoggable loggable, string message) {
            Debug.LogError(message);
        }

    }

}