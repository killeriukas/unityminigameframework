﻿
public interface IWeightedAttributeCalculator {
    string RollItemId();
    WeightedAttributeCalculator Clone();
}
