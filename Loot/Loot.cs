﻿using TMI.Mathematics;
using TMI.Number;

public struct Loot {

    public string type;
    public Chance chance;
    public double quantity;

    public Loot(string type, double quantity, Chance chance) {
        this.type = type;
        this.quantity = quantity;
        this.chance = chance;
    }

    public Loot(Currency currency, Chance chance) {
        this.type = currency.type;
        this.quantity = currency.amount;
        this.chance = chance;
    }

}
