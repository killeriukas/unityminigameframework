﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMI;

public class WeightedAttributeCalculator : IWeightedAttributeCalculator {

    private struct Map {
        public string itemId;
        public int weight;
    }

    private List<Map> itemIdsByWeight = new List<Map>();

    private int totalWeight = 0;

    public void Add(string itemId, int weight) {
        Assert.IsTrue<ArgumentException>(itemIdsByWeight.Any(x => x.itemId.Equals(itemId)), "Item is already in the weighted list! Id: " + itemId);
        Assert.IsFalse<ArgumentException>(weight > 0, "Weight cannot be less than or equal to 0! Id: " + itemId);

        totalWeight += weight;

        Map map = new Map();
        map.itemId = itemId;
        map.weight = weight;

        itemIdsByWeight.Add(map);
    }


    public string RollItemId() {
        int randomWeight = UnityEngine.Random.Range(0, totalWeight);

        List<Map> orderedWeightList = itemIdsByWeight.OrderBy(x => x.weight).ToList();
        int maskWeight = 0;
        foreach(Map map in orderedWeightList) {
            maskWeight += map.weight;
            if(randomWeight < maskWeight) {
                return map.itemId;
            }
        }
        throw new InvalidOperationException("Couldn't find a rolled value for such weight. Random weight: " + randomWeight + " Total: " + totalWeight);
    }

    public void RemoveItemId(string rolledItemGradeId) {
        Map foundMap = itemIdsByWeight.Find(x => x.itemId.Equals(rolledItemGradeId));
        totalWeight -= foundMap.weight;
        itemIdsByWeight.Remove(foundMap);
    }

    public WeightedAttributeCalculator Clone() {
        WeightedAttributeCalculator weightedLootCalculator = new WeightedAttributeCalculator();
        weightedLootCalculator.itemIdsByWeight = new List<Map>(itemIdsByWeight);
        weightedLootCalculator.totalWeight = totalWeight;
        return weightedLootCalculator;
    }
}
