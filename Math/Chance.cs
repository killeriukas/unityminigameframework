﻿
using Newtonsoft.Json;

namespace TMI.Mathematics {

    [JsonObject(MemberSerialization.OptIn)]
    public struct Chance {

        [JsonProperty]
		private float chance;

        public Chance(float chance) {
			Assert.IsTrue<System.ArgumentOutOfRangeException>(chance < 0f || chance > 1f, "Chance cannot be negative or > 1! Current: " + chance);
			this.chance = chance;
		}

        public static Chance operator+(Chance c1, Chance c2) {
            float totalChance = c1.chance + c2.chance;

            if(totalChance > 1f) {
                totalChance = 1f;
            }

            Chance newChance = new Chance(totalChance);
            return newChance;
        }

        public static bool operator>(Chance c1, Chance c2) {
			return c1.chance > c2.chance;
		}

		public static bool operator>=(Chance c1, Chance c2) {
			return c1.chance >= c2.chance;
		}

		public static bool operator<(Chance c1, Chance c2) {
			return c1.chance < c2.chance;
		}

		public static bool operator<=(Chance c1, Chance c2) {
			return c1.chance <= c2.chance;
		}

		public override string ToString() {
            return string.Format("{0:0%}", chance);
        }

        public string ToCustomString(string format) {
            return string.Format(format, chance);
        }

        public static Chance zero {
            get {
                return new Chance(0f);
            }
        }

        public static Chance hundred {
            get {
                return new Chance(1f);
            }
        }

        public static Chance FromDouble(double value) {
            return new Chance((float)value);
        }

        public float ToFloat() {
            return chance;
        }
    }


}