﻿using System;

namespace TMI.Mathematics {

    public class EaseDouble : EaseGeneric<double> {

        public EaseDouble(BaseInterpolator<double> interpolator, TimeSpan duration, Action onComplete) : base(interpolator, duration, onComplete) {
        }

    }

}