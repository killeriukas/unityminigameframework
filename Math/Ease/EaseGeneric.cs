﻿using System;
using TMI.Helper;

namespace TMI.Mathematics {

    public class EaseGeneric<TType, TReturnType> {

        private BaseInterpolator<TType, TReturnType> interpolator;

        private bool isDone = false;
        private readonly TimeSpan duration;
        private TimeSpan currentTimeStep = TimeSpan.Zero;

        private Action onComplete;

        public EaseGeneric(BaseInterpolator<TType, TReturnType> interpolator, TimeSpan duration, Action onComplete) {
            this.interpolator = interpolator;
            this.duration = duration;
            this.onComplete = onComplete;
        }

        public void Invert() {
            this.currentTimeStep = duration.Subtract(currentTimeStep);
            this.interpolator.Invert();
        }

        public void Invert(Action onComplete) {
            this.onComplete = onComplete;
            Invert();
        }

        public TReturnType Update(TimeSpan deltaTimeStep) {
            Assert.IsTrue<InvalidOperationException>(isDone, "Cannot update the easing parameters anymore, because it is completed!");

            currentTimeStep = currentTimeStep.Add(deltaTimeStep); 

            float currentTotalSeconds = (float)currentTimeStep.TotalSeconds;
            float durationSeconds = (float)duration.TotalSeconds;

            float normalizedTimeStep = currentTotalSeconds / durationSeconds;
            TimeSpan normalizedTimeSpan = TimeSpan.FromSeconds(normalizedTimeStep);

            if(normalizedTimeSpan.TotalSeconds < 1) {
                return interpolator.Calculate(normalizedTimeSpan);
            } else {
                isDone = true;
                onComplete?.Invoke();
                return interpolator.Calculate(normalizedTimeSpan);
            }
        }
    }

    public class EaseGeneric<TType> : EaseGeneric<TType, TType> {

        public EaseGeneric(BaseInterpolator<TType, TType> interpolator, TimeSpan duration, Action onComplete)
            : base(interpolator, duration, onComplete) {

        }

    }

}