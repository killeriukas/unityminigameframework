﻿using System;
using UnityEngine;

namespace TMI.Mathematics {

    public class EaseTransform<TReturnType> : EaseGeneric<Transform, TReturnType> {

        public EaseTransform(BaseInterpolator<Transform, TReturnType> interpolator, TimeSpan duration, Action onComplete) : base(interpolator, duration, onComplete) {
        }

    }

}