﻿using System;
using UnityEngine;

namespace TMI.Mathematics {

    public class EaseVector3 : EaseGeneric<Vector3> {

        public EaseVector3(BaseInterpolator<Vector3> interpolator, TimeSpan duration, Action onComplete) : base(interpolator, duration, onComplete) {
        }

    }

}