﻿
namespace TMI.Mathematics {

    public interface IInterpolation {
        float Convert(float value);
    }

}