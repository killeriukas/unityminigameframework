﻿using System;

namespace TMI.Mathematics {

    public abstract class BaseInterpolator<TType, TReturnType> {

        protected TType start { get; private set; }
        protected TType end { get; private set; }
        private readonly IInterpolation interpolation;

        protected BaseInterpolator(TType start, TType end, IInterpolation interpolation) {
            this.start = start;
            this.end = end;
            this.interpolation = interpolation;
        }

        public void Invert() {
            TType oldStart = start;
            start = end;
            end = oldStart;
        }

        public TReturnType Calculate(TimeSpan normalizedTime) {
            float normalizedSeconds = (float)normalizedTime.TotalSeconds;
            float convertedNormalizedSeconds = interpolation.Convert(normalizedSeconds);
            return Interpolate(convertedNormalizedSeconds);
        }

        protected abstract TReturnType Interpolate(float normalizedValue);
    }

    public abstract class BaseInterpolator<TType> : BaseInterpolator<TType, TType> {

        protected BaseInterpolator(TType start, TType end, IInterpolation interpolation)
            : base(start, end, interpolation) {

        }

    }

}