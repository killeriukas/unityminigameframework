﻿using System;
using TMI.Helper;

namespace TMI.Mathematics {

    public class InterpolatorDouble : BaseInterpolator<double> {

        public InterpolatorDouble(double startPosition, double endPosition, IInterpolation interpolation) : base(startPosition, endPosition, interpolation) {

        }

        protected override double Interpolate(float normalizedValue) {
            return MathHelper.Lerp(start, end, normalizedValue);
        }
    }

}