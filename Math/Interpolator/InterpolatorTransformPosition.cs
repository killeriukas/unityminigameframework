﻿using UnityEngine;

namespace TMI.Mathematics {

    public class InterpolatorTransformPosition : BaseInterpolator<Transform, Vector3> {

        public InterpolatorTransformPosition(Transform startTransform, Transform endTransform, IInterpolation interpolation)
            : base(startTransform, endTransform, interpolation) {

        }

        protected override Vector3 Interpolate(float normalizedValue) {
            return Vector3.Lerp(start.position, end.position, normalizedValue);
        }
    }

}