﻿using UnityEngine;

namespace TMI.Mathematics {

    public class InterpolatorVector3 : BaseInterpolator<Vector3> {

        public InterpolatorVector3(Vector3 startPosition, Vector3 endPosition, IInterpolation interpolation) : base(startPosition, endPosition, interpolation) {

        }

        protected override Vector3 Interpolate(float normalizedValue) {
            return Vector3.Lerp(start, end, normalizedValue);
        }
    }

}