﻿
namespace TMI.Mathematics {

    public class NumberCarousel {

        private readonly int limit;

        public int index { get; private set; }

        public NumberCarousel(int startingIndex, int limit) {
            this.index = startingIndex;
            this.limit = limit;
        }

        public NumberCarousel(int limit) : this(0, limit) {

        }

        public void Increase() {
            index++;

            index %= limit;
        }

        public void Decrease() {
            --index;

            if(index < 0) {
                index = limit - 1;
            }

        }

        public override string ToString() {
            return string.Format("{0}/{1}", index + 1, limit);
        }

    }

}


