﻿using System;
using Newtonsoft.Json;

namespace TMI.Mathematics {

    [JsonObject(MemberSerialization.OptIn)]
    public struct Percent {

        [JsonProperty]
		private float currentPercentage;

        public static Percent zero => new Percent(0f);
        public static Percent hundred => new Percent(1f);
        public static Percent twoHundred => new Percent(2f);
        public static Percent threeHundred => new Percent(3f);

        public Percent(float percent) {
			Assert.IsTrue<ArgumentOutOfRangeException>(percent < 0f, "Percent cannot be negative! Current: " + percent);
			this.currentPercentage = percent;
		}

        public static Percent operator +(Percent v1, Percent v2) {
            float newPercentage = v1.currentPercentage + v2.currentPercentage;
            Percent percent = new Percent(newPercentage);
            return percent;
        }

        public static Percent operator *(Percent v1, Percent v2) {
            float newPercentage = v1.currentPercentage * v2.currentPercentage;
            Percent percent = new Percent(newPercentage);
            return percent;
        }

        public static bool operator>(Percent v1, Percent v2) {
			return v1.currentPercentage > v2.currentPercentage;
		}

		public static bool operator>=(Percent v1, Percent v2) {
			return v1.currentPercentage >= v2.currentPercentage;
		}

		public static bool operator<(Percent v1, Percent v2) {
			return v1.currentPercentage < v2.currentPercentage;
		}

		public static bool operator<=(Percent v1, Percent v2) {
			return v1.currentPercentage <= v2.currentPercentage;
		}

        public static Percent operator -(Percent v1, Percent v2) {
            float newValue = v1.currentPercentage - v2.currentPercentage;
            Assert.IsTrue<ArgumentOutOfRangeException>(newValue < 0f, "Percent cannot be negative! Current: " + newValue);
            return new Percent(newValue);
        }

		public float Apply(float number) {
            return (float)Apply((double)number);
		}

		public float Apply(int number) {
            return (float)Apply((double)number);
		}

        public double Apply(double number) {
            double total = number * currentPercentage;
            return total;
        }

        public static Percent FromDouble(double value) {
            return new Percent((float)value);
        }

		public override string ToString() {
			return string.Format("{0:0%}", currentPercentage);
		}

        public string ToCustomString(string format) {
            return string.Format(format, currentPercentage);
        }

        public float ToFloat() {
            return currentPercentage;
        }

    }


}