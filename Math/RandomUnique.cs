﻿using System.Collections.Generic;
using UnityEngine;

namespace TMI.Mathematics {

    public class RandomUnique<TType> {

        private readonly List<TType> randomList;

        public RandomUnique(IEnumerable<TType> list) {
            this.randomList = new List<TType>(list);
        }

        public TType consumeRandomItem {
            get {
                int randomIndex = Random.Range(0, randomList.Count);
                TType itemFound = randomList[randomIndex];
                randomList.RemoveAt(randomIndex);
                return itemFound;
            }
        }

        public bool isEmpty {
            get {
                return 0 == randomList.Count;
            }
        }

    }

}


