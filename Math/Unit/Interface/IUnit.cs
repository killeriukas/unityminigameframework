﻿
namespace TMI.Mathematics {

    public interface IUnit {
        string description { get; }
        float ToFloat();
        bool isFull { get; }
    }

}
