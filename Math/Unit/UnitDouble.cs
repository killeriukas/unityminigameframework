﻿namespace TMI.Mathematics {

    public struct UnitDouble : IUnit {

        private double number;
        private double limit;
        private double parameter;

        public UnitDouble(double number, double limit) {
            Assert.IsTrue<System.ArgumentException>(number < 0 || number > limit, "Number cannot be negative or more than a limit! Number: " + number);
            Assert.IsTrue<System.ArgumentException>(limit < 0, "Limit cannot be negative! Limit: " + limit);

            this.number = number;
            this.limit = limit;
            this.parameter = number / limit;
        }

        public string description {
            get {
                return number + "/" + limit;
            }
        }

        public bool isFull {
            get {
                return parameter >= 1;
            }
        }

        public float ToFloat() {
            return (float)parameter;
        }

        public override string ToString() {
            return parameter.ToString("N2");
        }

    }


}