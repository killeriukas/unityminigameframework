﻿using TMI.Helper;

namespace TMI.Mathematics {

    public struct UnitFormattedDouble : IUnit {

        private double number;
        private double limit;
        private double parameter;

        public UnitFormattedDouble(double number, double limit) {
            Assert.IsTrue<System.ArgumentOutOfRangeException>(number < 0 || number > limit, "Number cannot be negative or more than a limit! Number: " + number + " Limit: " + limit);
            Assert.IsTrue<System.ArgumentOutOfRangeException>(limit < 0, "Limit cannot be negative! Limit: " + limit);

            this.number = number;
            this.limit = limit;
            this.parameter = number / limit;
        }

        public string description {
            get {
                return FormattingHelper.FormatDouble(number) + "/" + FormattingHelper.FormatDouble(limit);
            }
        }

        public bool isFull {
            get {
                return parameter >= 1;
            }
        }

        public float ToFloat() {
            return (float)parameter;
        }

        public override string ToString() {
            return parameter.ToString("N2");
        }

    }


}