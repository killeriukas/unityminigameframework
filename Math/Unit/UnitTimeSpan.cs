﻿using System;

namespace TMI.Mathematics {

    public struct UnitTimeSpan : IUnit {

        private TimeSpan number;
        private TimeSpan limit;
        private double parameter;

        public UnitTimeSpan(TimeSpan number, TimeSpan limit, bool inverse) {
            Assert.IsTrue<ArgumentOutOfRangeException>(limit == TimeSpan.Zero, "Limit timespan cannot be zero when creating UnitTimeSpan.");

            if(number < TimeSpan.Zero) {
                number = TimeSpan.Zero;
            }

            if(number > limit) {
                number = limit;
            } 

            this.number = number;
            this.limit = limit;

            if(inverse) {
                this.parameter = 1f - number.TotalSeconds / limit.TotalSeconds;
            } else {
                this.parameter = number.TotalSeconds / limit.TotalSeconds;
            }

            this.isFull = this.parameter >= 1f;
        }

        public bool isFull { get; private set; }

        public string description {
            get {
                return number.TotalSeconds + "/" + limit.TotalSeconds;
            }
        }

        public float ToFloat() {
            return (float)parameter;
        }

        public override string ToString() {
            return parameter.ToString("N2");
        }

    }


}