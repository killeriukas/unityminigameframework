﻿using Newtonsoft.Json;

namespace TMI {

    [JsonObject(MemberSerialization.OptIn)]
    public struct Version {

        [JsonProperty]
        private readonly int major;

        [JsonProperty]
        private readonly int medium;

        [JsonProperty]
        private readonly int minor;

        public Version(int major, int medium, int minor) {
            this.major = major;
            this.medium = medium;
            this.minor = minor;
        }

        public Version(string version) {
            string[] split = version.Split('.');

            if(3 != split.Length) {
                throw new System.ArgumentOutOfRangeException("The version has to be like this X.X.X -> 0.0.0; You've passed: " + version);
            }

            this.major = int.Parse(split[0]);
            this.medium = int.Parse(split[1]);
            this.minor = int.Parse(split[2]);
        }

        public override bool Equals(object obj) {
            if(obj == null || !(obj is Version)) {
                return false;
            }

            Version v = (Version)obj;

            bool isEqual = IsEqual(this, v);
            return isEqual;
        }

        private static bool IsEqual(Version v1, Version v2) {
            return v1.major == v2.major && v1.medium == v2.medium && v1.minor == v2.minor;
        }

        public static bool operator >(Version v1, Version v2) {
            return v1.major > v2.major || v1.medium > v2.medium || v1.minor > v2.minor;
        }

        public static bool operator <(Version v1, Version v2) {
            return v1.major < v2.major || v1.medium < v2.medium || v1.minor < v2.minor;
        }

        public static bool operator ==(Version v1, Version v2) {
            return IsEqual(v1, v2);
        }

        public static bool operator !=(Version v1, Version v2) {
            return !IsEqual(v1, v2);
        }

        public override int GetHashCode() {
            return ToString().GetHashCode();
        }

        public override string ToString() {
            return string.Format("{0}.{1}.{2}", major, medium, minor);
        }

    }


}