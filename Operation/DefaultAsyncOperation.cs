﻿using System;

namespace TMI.Operation {

    public class DefaultAsyncOperation<TAsset> : IAsyncOperation<TAsset>, IAsyncOperationExecutor<TAsset> {

		#region IAsyncOperation implementation
		public event Action<TAsset> onCompleted;
		public event Action onCanceled;
		public event Action onFailed;
		#endregion

		private DefaultAsyncOperation() {

		}

		private DefaultAsyncOperation(Action<TAsset> completedCallback) {
			onCompleted += completedCallback;
		}

		public static IAsyncOperation<TAsset> Create(Action<TAsset> completedCallback) {
			return new DefaultAsyncOperation<TAsset>(completedCallback);
		}

		public static IAsyncOperation<TAsset> Create() {
			return new DefaultAsyncOperation<TAsset>();
		}

		#region IAsyncOperationExecutor implementation
		public void Complete(TAsset loadedAsset) {
            onCompleted?.Invoke(loadedAsset);
		}
		public void Cancel() {
            onCanceled?.Invoke();
		}
		public void Fail() {
            onFailed?.Invoke();
		}
		#endregion
	}

}