﻿using UnityEngine;
using TMI.AssetManagement;

namespace TMI.Operation {

	public class LoadAssetGroupHandle : BaseGroupHandle<Object, AssetGroup, IAssetGroupComplete> {

		public LoadAssetGroupHandle(IAsyncOperationExecutor<IAssetGroupComplete> asyncOperationExecutor, AssetGroup assetGroup) : base(asyncOperationExecutor, assetGroup) {
			
		}

	}

}