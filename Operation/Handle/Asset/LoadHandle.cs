﻿using TMI.AssetManagement;

namespace TMI.Operation {
	
	public class LoadHandle<TAsset> : BaseHandle<TAsset> {

		private readonly IAsyncOperationExecutor<TAsset> asyncOperationExecutor;

		public LoadHandle(IAsyncOperationExecutor<TAsset> asyncOperationExecutor) {
			this.asyncOperationExecutor = asyncOperationExecutor;
		}

		sealed public override void SetProgress(ILoader loader) {
			progress = loader.progress;
		}

		sealed public override void Complete(IAssetLoader<TAsset> loader) {
			progress = 1f;
			isComplete = true;

			TAsset loadedAsset = loader.asset;
			asyncOperationExecutor.Complete(loadedAsset);
		}

		sealed protected override void OnCancel() {
			asyncOperationExecutor.Cancel();
		}

	}

}