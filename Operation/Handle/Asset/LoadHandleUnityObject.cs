﻿using TMI.AssetManagement;
using UnityEngine;

namespace TMI.Operation {
	public class LoadHandleUnityObject<TAsset> : LoadHandle<GenericUnityAsset<TAsset>> where TAsset : Object {

		public LoadHandleUnityObject(IAsyncOperationExecutor<GenericUnityAsset<TAsset>> asyncOperationExecutor) : base(asyncOperationExecutor) {

		}

	}

}