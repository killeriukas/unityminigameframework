﻿using TMI.AssetManagement;

namespace TMI.Operation {

	public abstract class BaseGroupHandle<TAsset, TGroup, TAssetGroupComplete> : BaseHandle<TAsset> 
		where TGroup : BaseGroup<TAsset>, TAssetGroupComplete
		where TAssetGroupComplete : IGroupComplete<TAsset> {

		private readonly TGroup group;
		private readonly IAsyncOperationExecutor<TAssetGroupComplete> asyncOperationExecutor;

		protected BaseGroupHandle(IAsyncOperationExecutor<TAssetGroupComplete> asyncOperationExecutor, TGroup group) {
			this.asyncOperationExecutor = asyncOperationExecutor;
			this.group = group;
		}

		sealed public override void SetProgress(ILoader loader) {
			group.SetProgress(loader.path, loader.progress);
			progress = group.progress;
		}

		sealed public override void Complete(IAssetLoader<TAsset> loader) {
			TAsset loadedAsset = loader.asset;
			group.LoadComplete(loader.path, loadedAsset);

			if(group.isComplete) {
				isComplete = true;
				progress = 1f;
				asyncOperationExecutor.Complete(group);
			}
		}

		sealed protected override void OnCancel () {
			asyncOperationExecutor.Cancel();
		}

	}

}