﻿using TMI.AssetManagement;

namespace TMI.Operation {

	public abstract class BaseHandle<TAsset> : IHandle {

		protected bool isComplete;
		public bool isCanceled { get; private set; }
		public float progress { get; protected set; }

		protected BaseHandle() {
			this.progress = 0f;
			this.isCanceled = false;
			this.isComplete = false;
		}

		public abstract void SetProgress(ILoader loader);
		public abstract void Complete(IAssetLoader<TAsset> loader);

		protected abstract void OnCancel();

		public bool Cancel() {
			if(!isComplete && !isCanceled) {
				isCanceled = true;
				OnCancel();
				return true;
			}

			return false;
		}

	}
}