﻿using UnityEngine;
using TMI.AssetManagement;
using UnityEngine.SceneManagement;

namespace TMI.Operation {

	public class LoadSceneGroupHandle : BaseGroupHandle<Scene, SceneGroup, ISceneGroupComplete> {

		public LoadSceneGroupHandle(IAsyncOperationExecutor<ISceneGroupComplete> asyncOperationExecutor, SceneGroup sceneGroup) : base(asyncOperationExecutor, sceneGroup) {

		}

	}

}