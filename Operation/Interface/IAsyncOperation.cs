﻿using System;

namespace TMI.Operation {

	public interface IAsyncOperation<TAsset> {
		event Action<TAsset> onCompleted;
		event Action onCanceled;
		event Action onFailed;
	}

}

