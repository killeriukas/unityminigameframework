﻿namespace TMI.Operation {

    public interface IAsyncOperationExecutor<TAsset> {
		void Complete(TAsset loadedAsset);
		void Cancel();
		void Fail();
	}

}