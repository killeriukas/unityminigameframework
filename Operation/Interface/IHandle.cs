﻿
namespace TMI.Operation {
	
	public interface IHandle {
		float progress { get; }
		bool Cancel();
	}

}