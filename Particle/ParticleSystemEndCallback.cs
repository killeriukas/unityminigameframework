﻿using System;
using TMI.Helper;
using UnityEngine;

namespace TMI.ParticleManagement {

    public class ParticleSystemEndCallback : MonoBehaviour {

        private void Awake() {
            ParticleSystem particleSystem = GetComponent<ParticleSystem>();
            ParticleSystem.MainModule mainModule = particleSystem.main;
            mainModule.stopAction = ParticleSystemStopAction.Callback;
        }

        public event Action onParticleSystemStopped;

        private void OnParticleSystemStopped() {
            GeneralHelper.CallNullCheck(onParticleSystemStopped);
        }

    }

}