﻿namespace TMI.Pattern {

    public interface IPoolable {
        void Initialize();
        void Uninitialize();
    }

}