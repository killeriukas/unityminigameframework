﻿
using System;

namespace TMI.Pattern {

    public interface ISubject<TType> where TType : struct {
        TType value { get; }
        event Action<TType, TType> onChange;
    }

}