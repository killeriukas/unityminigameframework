﻿using System;
using TMI.Helper;

//re-think this later on
#if USE_NEWTONSOFT_JSON
using Newtonsoft.Json;
#endif

namespace TMI.Pattern {
    //re-think this later on
    #if USE_NEWTONSOFT_JSON
    [JsonObject(MemberSerialization.OptIn)]
    #endif
    public class Subject<TType> : ISubject<TType> where TType : struct {
            //re-think this later on
        #if USE_NEWTONSOFT_JSON
        [JsonProperty]
        #endif
        private TType _value;

        public Subject() {
            _value = default(TType);
        }

        public TType value {
            get {
                return _value;
            }
            set {
                TType oldValue = _value;
                _value = value;
                onChange?.Invoke(oldValue, _value);
            }
        }

        public event Action<TType, TType> onChange;

        public override string ToString() {
            return _value.ToString();
        }

    }

}