﻿using TMI.Core;
using TMI.Helper;
using TMI.Save;

public abstract class BaseSaveManager : BaseManager, ISerializer {

    public static bool useEncrypting = true;

    public T Load<T>() {
        string allReadFile = FileHelper.ReadWithBackup(saveFilePath);

        if(useEncrypting) {
            allReadFile = Decrypt(allReadFile);
        }

        T deserialiedObject = SerializationHelper.Deserialize<T>(allReadFile);
        return deserialiedObject;
    }

    public void Serialize(object serializeObject) {
        string saveObjectJson = SerializationHelper.Serialize(serializeObject);

        if(useEncrypting) {
            saveObjectJson = Encrypt(saveObjectJson);
        }

        FileHelper.WriteWithBackup(saveFilePath, saveObjectJson);
    }

    protected abstract string Decrypt(string data);
    protected abstract string Encrypt(string data);
    protected abstract string saveFilePath { get; }


}
