﻿using TMI.Core;

namespace TMI.Save {

    public interface ISerializer : IManager {
        void Serialize(object serializeObject);
    }

}