﻿using Newtonsoft.Json;

namespace TMI.Save {

    [JsonObject(MemberSerialization.OptIn)]
    public abstract class BaseModelGeneric<TModel> : ISaveable<TModel> where TModel : IModel, new() {

        [JsonProperty]
        protected TModel model { get; private set; }

        protected BaseModelGeneric() {
            model = new TModel();
        }

        public virtual void Load(TModel model) {
            this.model = model;
        }

        public virtual void Save(out TModel model) {
            model = this.model;
        }

    }

}