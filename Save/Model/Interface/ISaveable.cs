﻿
namespace TMI.Save {

    public interface ISaveable<TModel> where TModel : IModel {
        void Save(out TModel model);
        void Load(TModel model);
    }

}