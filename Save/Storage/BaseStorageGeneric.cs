﻿using Newtonsoft.Json;

namespace TMI.Save {

    [JsonObject(MemberSerialization.OptIn)]
    public abstract class BaseStorageGeneric<TModel> : IStorage<TModel> where TModel : IModel, new() {

        [JsonProperty]
        protected TModel model { get; private set; }

        protected BaseStorageGeneric() {
            model = new TModel();
        }

        public virtual void Load(TModel model) {
            this.model = model;
        }

        public abstract void Save(ISerializer serializer);
	}

}