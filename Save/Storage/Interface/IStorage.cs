﻿
namespace TMI.Save {

    public interface IStorage<TModel> where TModel : IModel {
        void Save(ISerializer serializer);
        void Load(TModel model);
    }

}