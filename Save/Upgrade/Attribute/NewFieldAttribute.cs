﻿using System;

namespace TMI.Save {

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class NewFieldAttribute : Attribute {

        public readonly bool overwriteEveryTime;

        public NewFieldAttribute(bool overwriteEveryTime, string version) {
            this.overwriteEveryTime = overwriteEveryTime;
        }

    }


}