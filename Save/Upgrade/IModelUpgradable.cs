﻿
namespace TMI.Save {

    public interface IModelUpgradable<TModel> where TModel : class, IModel {
        void Upgrade(TModel oldModel);
    }

}