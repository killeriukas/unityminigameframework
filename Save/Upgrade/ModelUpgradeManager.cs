﻿using System;
using System.Collections.Generic;
using System.Reflection;
using TMI.Helper;

namespace TMI.Save {

    public class ModelUpgradeManager {

        public static TModel Upgrade<TModel>(TModel oldModel, TModel newModel) where TModel : class, IModel {

            //find new properties and initialize those
            List<FieldInfo> newFields = ReflectionHelper.FindAllClassFieldsWithAttribute<TModel, NewFieldAttribute>();
            Type attributeType = typeof(NewFieldAttribute);

            //initializing new fields, by changing new model values on the old model
            foreach(FieldInfo field in newFields) {
                object[] allAttributes = field.GetCustomAttributes(attributeType, false);
                NewFieldAttribute newFieldAttribute = (NewFieldAttribute)allAttributes[0];

                if(newFieldAttribute.overwriteEveryTime) {
                    object newValue = field.GetValue(newModel);
                    field.SetValue(oldModel, newValue);
                } else {
                    object oldValue = field.GetValue(oldModel);
                    if(oldValue == null) {
                        object newValue = field.GetValue(newModel);
                        field.SetValue(oldModel, newValue);
                    }
                }
            }

            //find obsolete properties and change them with the new ones
            //TODO: implement this one down the line


            return oldModel;
        }
        

    }


}