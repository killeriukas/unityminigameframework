﻿using TMI.Operation;
using TMI.AssetManagement;
using TMI.Core;

namespace TMI.SceneManagement {

    public interface ISceneManager : IManager {
        IHandle LoadScene(string sceneName);
        IHandle LoadSceneGroup(ISceneGroup sceneGroup, IAsyncOperation<ISceneGroupComplete> asyncOperation);
	}
}