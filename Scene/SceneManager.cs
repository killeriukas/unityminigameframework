﻿using TMI.Core;
using TMI.Operation;
using TMI.AssetManagement;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;
using TMI.Helper;

namespace TMI.SceneManagement {

    public class SceneManager : BaseManager, ISceneManager {

        private void OnSceneLoaded(Scene newScene) {
            Scene oldScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();

            GameObject[] newRootGameObjects = newScene.GetRootGameObjects();
            HierarchyHelper.FindAndDeleteEventSystem(newRootGameObjects);

            GameObject[] rootGameObjects = oldScene.GetRootGameObjects();
            CoreManager coreManager = FindCoreManager(rootGameObjects);

            UnityEngine.SceneManagement.SceneManager.MoveGameObjectToScene(coreManager.gameObject, newScene);
            UnityEngine.SceneManagement.SceneManager.SetActiveScene(newScene);
            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(oldScene);
        }

        private CoreManager FindCoreManager(GameObject[] gameObjectsArray) {
            foreach(GameObject go in gameObjectsArray) {
                CoreManager coreManager = go.GetComponent<CoreManager>();
                if(coreManager != null) {
                    return coreManager;
                }
            }

            throw new InvalidOperationException("CoreManager was not found! Something went horribly wrong!");
        }

        public IHandle LoadScene(string sceneName) {

            IAsyncOperation<Scene> asyncOperation = DefaultAsyncOperation<Scene>.Create(OnSceneLoaded);

            //we can do that because we always know that async operation is actually an executor. But be careful!
            IAsyncOperationExecutor<Scene> asyncOperationExecutor = (IAsyncOperationExecutor<Scene>)asyncOperation;

            SceneLoader sceneLoader = SceneLoader.LoadAsync(sceneName);
            LoadHandle<Scene> handle = new LoadHandle<Scene>(asyncOperationExecutor);

            StartCoroutine(RunCoroutine(sceneLoader, handle));
            return handle;
        }

        public IHandle LoadSceneGroup(ISceneGroup sceneGroup, IAsyncOperation<ISceneGroupComplete> asyncOperation) {

			//we can do that because we always know that async operation is actually an executor. But be careful!
			IAsyncOperationExecutor<ISceneGroupComplete> asyncOperationExecutor = (IAsyncOperationExecutor<ISceneGroupComplete>)asyncOperation;

			//we can do that because we always know that asset group interface is actually a full asset group. But be careful!
			SceneGroup fullSceneGroup = (SceneGroup)sceneGroup;

            Assert.IsTrue<InvalidOperationException>(fullSceneGroup.isComplete, "Cannot load scene group which is already complete, or empty to begin with!");

            LoadSceneGroupHandle handle = new LoadSceneGroupHandle(asyncOperationExecutor, fullSceneGroup);

			foreach(string sceneName in fullSceneGroup.paths) {
				SceneLoader sceneLoader = SceneLoader.LoadAsync(sceneName);
				StartCoroutine(RunCoroutine(sceneLoader, handle));
			}

			return handle;
		}

		private IEnumerator RunCoroutine(SceneLoader sceneLoader, BaseHandle<Scene> handle) {
			//never load on the first frame, because it's an async operation after all
			yield return null;

			while(!sceneLoader.isDone) {
				handle.SetProgress(sceneLoader);
				yield return null;
			}

			if(handle.isCanceled) {
				sceneLoader.Unload();
			} else {
				handle.Complete(sceneLoader);
			}

		}


	}


}