using System;
using TMI.AudioManagement;
using TMI.ProfileManagement;
using TMI.Save;
using Newtonsoft.Json;

namespace TMI.SettingsManagement {

	[JsonObject(MemberSerialization.OptIn)]
	public class Settings : BaseStorageGeneric<Settings.Data>, IProfile {

		[Serializable]
		public class Data : IModel {
			public Audio.Data audio;
		}

		private Audio _audio = new Audio();

		public IAudio Audio => _audio;

		public override void Load(Data model) {
			base.Load(model);
			_audio.Load(model.audio);
		}

		public override void Save(ISerializer serializer) {
			serializer.Serialize(model);
		}

	}


}

