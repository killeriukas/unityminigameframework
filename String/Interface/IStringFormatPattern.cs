﻿
namespace TMI.StringFormatManagement {

    public interface IStringFormatPattern {
        string GenerateString(params object[] arguments);
    }

}