﻿
using TMI.Helper;

namespace TMI.StringFormatManagement {

    public class StringFormatBigNumber : IStringFormatPattern {

        private string template;

        public StringFormatBigNumber(string template) {
            this.template = template;
        }

        public string GenerateString(params object[] arguments) {
            double bigNumber = (double)arguments[0];
            string formattedBigNumebr = FormattingHelper.FormatDouble(bigNumber);

            string formattedString = string.Format(template, formattedBigNumebr);
            return formattedString;
        }

    }

}