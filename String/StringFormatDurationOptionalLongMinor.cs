﻿
using System;
using TMI.Helper;

namespace TMI.StringFormatManagement {

    public class StringFormatDurationOptionalLongMinor : IStringFormatPattern {

        private string template;

        public StringFormatDurationOptionalLongMinor(string template) {
            this.template = template;
        }

        public string GenerateString(params object[] arguments) {
            double bigNumber = (double)arguments[0];
            TimeSpan duration = TimeSpan.FromSeconds(bigNumber);

            string formattedBigNumebr = FormattingHelper.FormatDurationOptionalLongMinor(duration);

            string formattedString = string.Format(template, formattedBigNumebr);
            return formattedString;
        }

    }

}