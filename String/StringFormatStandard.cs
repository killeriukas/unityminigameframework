﻿
namespace TMI.StringFormatManagement {

    public class StringFormatStandard : IStringFormatPattern {

        private string template;

        public StringFormatStandard(string template) {
            this.template = template;
        }

        public string GenerateString(params object[] arguments) {
            string formattedString = string.Format(template, arguments);
            return formattedString;
        }

    }

}