﻿using System.Collections.Generic;
using System.Text;
using TMI;
using System;

public class MegaNumberConverter {

	private readonly Dictionary<string, int> sequenceByRankId = new Dictionary<string, int>();
	private readonly List<string> rankIdByIndex = new List<string>();

	private const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static readonly int alphabetLength = alphabet.Length;

    private void AddNewPowerId(string newPowerId) {
        rankIdByIndex.Add(newPowerId);
        sequenceByRankId.Add(newPowerId, rankIdByIndex.Count - 1);
    }

	public MegaNumberConverter(int numberPower) {
        AddNewPowerId("");
        AddNewPowerId("K");
        AddNewPowerId("M");
        AddNewPowerId("B");
        AddNewPowerId("T");

		int numberOfLetters = 2;
		int letterIndex = 0;
		for(int i = sequenceByRankId.Count; i < numberPower; ++i) {

			if (letterIndex == alphabetLength) {
				++numberOfLetters;
				letterIndex = 0;
			}

            string newNumberPowerId = "";
			for(int j = 0; j < numberOfLetters; ++j) {
				newNumberPowerId += alphabet[letterIndex];
			}

            AddNewPowerId(newNumberPowerId);

			++letterIndex;
		}

	}

	public IEnumerable<string> GetAllRankIdsSortedUpToIndex(int highestPower, bool ascending) {
        List<string> allPowerIdsUpToIndex = rankIdByIndex.GetRange(0, highestPower);

		if(!ascending) {
			allPowerIdsUpToIndex.Reverse();
		}

        return allPowerIdsUpToIndex;
	}

	public IEnumerable<string> GetAllRankIdsUpToRankId(string rankId, bool ascending) {
		int rankIndex = sequenceByRankId[rankId];
		return GetAllRankIdsSortedUpToIndex(rankIndex, ascending);
	}

	public List<string> GetLastNRankIdsFromRankIndex(int amount, int highestRankIndex) {
        int cutFromIndex = highestRankIndex - amount;
		List<string> lastRankIds = rankIdByIndex.GetRange(cutFromIndex, amount);
        return lastRankIds;
    }

	public string GetNextRankId(string rankId) {
		int currentRankIndex = sequenceByRankId[rankId];
		string nextRankId = rankIdByIndex[currentRankIndex + 1];
		return nextRankId;
	}

//	public bool IsMore(string id1, string id2) {
//		bool isMore = sequenceById [id1] > sequenceById [id2];
//		return isMore;
//	}



}


public class MegaNumber {

	public static bool isDebugMode = false;

	private MegaNumberConverter converter;

	private Dictionary<string, int> numberByRankId = new Dictionary<string, int>();

    public MegaNumber(MegaNumberConverter converter) {
        this.converter = converter;
		numberByRankId.Add("", 0);
	}

    public MegaNumber(MegaNumberConverter converter, int number) {
        Assert.IsFalse<ArgumentOutOfRangeException>(number > 0 && number < 1000, "Argument cannot be negative or higher than 999! If it's more - use Add(MegaNumber) instead! Number: " + number);
        this.converter = converter;
		numberByRankId.Add("", number);
	}

	public MegaNumber(MegaNumberConverter converter, RankedNumber rankedNumber) {
		Assert.IsFalse<ArgumentOutOfRangeException>(rankedNumber.amount > 0 && rankedNumber.amount < 1000, "Argument cannot be negative or higher than 999! If it's more - use Add(MegaNumber) instead! Number: " + rankedNumber.amount);
		this.converter = converter;

		IEnumerable<string> allRankIds = converter.GetAllRankIdsUpToRankId(rankedNumber.type, true);

		foreach(string rankId in allRankIds) {
			numberByRankId.Add(rankId, 0);
			UnityEngine.Debug.LogError("rankid: " + rankId);
		}

		numberByRankId.Add(rankedNumber.type, rankedNumber.amount);
	}

	public void Subtract(MegaNumber number) {
		int highestRankIndex = number.numberByRankId.Count;

		//TODO: do more

	}

	public void Add(MegaNumber number) {
		int highestRankIndex = number.numberByRankId.Count;

		IEnumerable<string> allRankIds = converter.GetAllRankIdsSortedUpToIndex(highestRankIndex, true);
		foreach(string rankId in allRankIds) {
            int newAmount = number.numberByRankId[rankId];

	//		AddMegaNumber(rankId, newAmount);

			if(numberByRankId.ContainsKey(rankId)) {
				newAmount += numberByRankId[rankId];
			}

			if (newAmount < 1000) {
                numberByRankId[rankId] = newAmount;
			} else {
                ConvertMoreThanThousand(rankId, newAmount);
			}

		}

	}

//	private void AddMegaNumber(string numberRankId, int number) {
//		if(numberByRankId.ContainsKey(numberRankId)) {
//			int newRankNumber = number / 1000;
//			string nextRankId = converter.GetNextRankId(numberRankId);
//			int newAmount = newRankNumber + numberByRankId[nextRankId];
//
//			if(newAmount < 1000) {
//				numberByRankId[nextRankId] = newAmount;
//			} else {
//				int leftOverNumber = number % 1000;
//				numberByRankId[numberRankId] = leftOverNumber;
//				AddMegaNumber(nextRankId, newAmount);
//			}
//		} else {
//			numberByRankId.Add(numberRankId, number);
//		}
//	}

	private void ConvertMoreThanThousand(string numberRankId, int number) {
        Assert.IsTrue<ArgumentOutOfRangeException>(number < 1000, "Cannot convert a number which is less than 1000! Number: " + number);

        UnityEngine.Debug.LogError("add: " + number + " rank: " + numberRankId);

		int newRankNumber = number / 1000;

		int leftOver = number % 1000;

		numberByRankId[numberRankId] = leftOver;

		string nextRankId = converter.GetNextRankId(numberRankId);

		if(numberByRankId.ContainsKey(nextRankId)) {
			int newAmount = newRankNumber + numberByRankId[nextRankId];
			if(newAmount < 1000) {
				numberByRankId[nextRankId] = newAmount;
			} else {
				ConvertMoreThanThousand(nextRankId, newAmount);
			}
		} else {
			numberByRankId.Add(nextRankId, newRankNumber);
		}

    }

    public override string ToString() {
		int numberRank = numberByRankId.Count;

        if(numberRank > 1) {
			if(isDebugMode) {
				StringBuilder stringBuilder = new StringBuilder();

				IEnumerable<string> allRankIds = converter.GetAllRankIdsSortedUpToIndex(numberRank, false);
				foreach(string rankId in allRankIds) {
					int amount = numberByRankId[rankId];
					if(amount < 10) {
						stringBuilder.Append("00");	
					} else if (amount < 100) {
						stringBuilder.Append("0");	
					}
					stringBuilder.Append(amount);
					stringBuilder.Append(rankId);
					stringBuilder.Append(',');
				}

				return stringBuilder.ToString();
			} else {
				List<string> lastTwoRankIds = converter.GetLastNRankIdsFromRankIndex(2, numberRank);

				string lowerRankId = lastTwoRankIds[0];
				string higherRandId = lastTwoRankIds[1];

				string secondHalfString = null;
				int secondNumber = numberByRankId[lowerRankId];
				if(secondNumber >= 10 && secondNumber < 100) {
					secondHalfString = ".0" + secondNumber.ToString();
				} else if (secondNumber > 100) {
					secondHalfString = secondNumber.ToString();
					secondHalfString = "." + secondHalfString.Substring(0, 2);
				}

				string firstHalfString = numberByRankId[higherRandId].ToString();
				string finalNumberString = firstHalfString + secondHalfString + higherRandId;
				return finalNumberString;
			}
        }
        else {
            return numberByRankId[""].ToString();
        }
    }

}
