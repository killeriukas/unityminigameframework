﻿
public struct RankedNumber {

	public readonly int amount;
	public readonly string type;

	public RankedNumber(string type, int amount) {
		this.type = type;
		this.amount = amount;
	}

}
