﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMI.Core;
using TMI.Helper;
using TMI.Notification;
using TMI.UI.Extension;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI {

	public abstract class BaseUIController : BaseNotificationBehaviour {

        private enum State {
            Unknown,
            Showing,
            Hidden
        }

        private Canvas mainCanvas;
        private Canvas[] subCanvases;
        private bool isRefreshed = false;

        private State currentState = State.Unknown;

        private UIAnimation[] uiAnimations;

        protected IUIManager uiManager { get; private set; }

        public bool isShowing { get { return currentState == State.Showing; } }

        protected override void Awake() {
            base.Awake();
            mainCanvas = GetComponent<Canvas>();
            Assert.IsFalse<ArgumentException>(mainCanvas.renderMode == RenderMode.ScreenSpaceCamera, "Camera render mode must be set to ScreenSpaceCamera! Scene: " + sceneName);

            subCanvases = this.GetComponentsInChildrenOnly<Canvas>(true);
        }

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            uiManager = initializer.GetManager<UIManager, IUIManager>();

            //initialize all components excluding cells and their children
            //cells are initialized when they are cloned
            IEnumerable<IUIComponent> uiComponentsWithoutCells = HierarchyHelper.GetComponentsInChildrenOnlyExcludingOne<IUIComponent, Cell>(this);
            foreach(IUIComponent component in uiComponentsWithoutCells) {
                component.Setup(initializer);
            }

        }

        protected override void Start() {
            base.Start();
            CoreManager coreManager = CoreManager.Create();
            mainCanvas.worldCamera = coreManager.uiCamera;

            uiAnimations = GetComponentsInChildren<UIAnimation>(true);
        }

        public virtual void OnUIReady() {
        }

        public virtual void Show() {
            Assert.IsFalse<InvalidOperationException>(isRefreshed, "Cannot Show() popup before Refresh() is called!");

            //   Assert.IsTrue<InvalidOperationException>(currentState == State.Showing, "Cannot show the UIController, when it's already being shown!");
            currentState = State.Showing;

            gameObject.SetActive(true);

            foreach(UIAnimation uiAnimation in uiAnimations) {
                if(uiAnimation.playAnimationOnShow) {
                    uiAnimation.Play();
                }
            }

            Trigger(new UISceneShownNotification(sceneName));
		}

		public virtual void Hide() {
         //   Assert.IsTrue<InvalidOperationException>(currentState == State.Hidden, "Cannot hide the UIController, when it's already hidden!");
            gameObject.SetActive(false);
            currentState = State.Hidden;

            isRefreshed = false;

            Trigger(new UISceneHiddenNotification(sceneName));
        }

		protected void Close(PointerEventData eventData) {
			Hide();
		}

        public virtual void Refresh() {
            int selectedMainCanvasSortingOrder = mainCanvas.sortingOrder;
            string mainCanvasSortingLayer = mainCanvas.sortingLayerName;

            int mainCanvasSortingOrder = uiManager.CreateSortingOrder(mainCanvas.sortingLayerName);
            mainCanvas.sortingOrder = mainCanvasSortingOrder;

            foreach(Canvas subCanvas in subCanvases) {
                if(subCanvas.overrideSorting) {
                    Assert.IsFalse<InvalidOperationException>(mainCanvasSortingLayer.Equals(subCanvas.sortingLayerName), "Canvas cannot have two different sorting layers! Use SortingOrder instead! Main layer: " + mainCanvasSortingLayer + " Second layer: " + subCanvas.sortingLayerName);

                    int selectedSubCanvasSortingOrder = subCanvas.sortingOrder;
                    int sortingOrderRelativeDifference = selectedSubCanvasSortingOrder - selectedMainCanvasSortingOrder;

                    subCanvas.sortingOrder = uiManager.CreateSortingOrder(subCanvas.sortingLayerName, sortingOrderRelativeDifference);
                }
            }

            isRefreshed = true;
        }

	}

}