﻿using TMI.LogManagement;
using UnityEngine;

namespace TMI.UI {

    [RequireComponent(typeof(Animator))]
    public class UIAnimator : UIComponentWithId<Animator> {

        [SerializeField]
        private bool keepAnimatorStateOnDisable = false;

        protected override void OnInitialize() {
            base.OnInitialize();

            component.keepAnimatorControllerStateOnDisable = keepAnimatorStateOnDisable;
        }

        public void SetBool(string id, bool value) {
            Logging.Log(this, "Animator SetBool() [{0}] with value [{1}] on [{2}].", id, value, gameObject.name);
            component.SetBool(id, value);
        }

    }

}