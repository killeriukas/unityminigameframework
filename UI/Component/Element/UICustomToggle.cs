﻿using System;
using TMI.UI.Extension;
using UnityEngine;

namespace TMI.UI {

    [RequireComponent(typeof(CustomToggle))]
    public class UICustomToggle : UIComponentWithId<CustomToggle> {

        public event Action<bool, bool> onValueChanged;

        protected override void OnInitialize() {
            base.OnInitialize();
            component.onValueChanged.AddListener(OnToggleValueChanged);
        }

        public bool isOn {
            get {
                return component.isOn;
            }
            set {
                component.isOn = value;
            }
        }

        private void OnToggleValueChanged(bool changedByGroup, bool currentValue) {
            onValueChanged?.Invoke(changedByGroup, currentValue);
        }

        public void ClearOnValueChanged() {
            onValueChanged = null;
        }

        protected override void OnDestroy() {
            onValueChanged = null;
            component.onValueChanged.RemoveAllListeners();
            base.OnDestroy();
        }

    }

}