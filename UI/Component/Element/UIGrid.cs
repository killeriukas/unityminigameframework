﻿using UnityEngine;
using TMI.UI.Extension;
using System;

namespace TMI.UI {

    [RequireComponent(typeof(Extension.Grid))]
    public class UIGrid : UIComponentWithId<Extension.Grid> {

        public Cell CloneCell() {
            return component.CloneCell();
        }

        public int GetCellHorizontalIndex(Cell cell) {
            return component.GetCellHorizontalIndex(cell);
        }

        public void DestroyCell(Cell cell) {
            component.DestroyCell(cell);
        }

    }
}