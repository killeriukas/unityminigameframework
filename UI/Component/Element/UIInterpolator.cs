﻿using TMI.UI.Extension;
using UnityEngine;

namespace TMI.UI {

    [RequireComponent(typeof(Interpolator))]
    public class UIInterpolator : UIComponentWithId<Interpolator> {

		public double number {
            set {
                component.number = value;
            }
        }

	}

}