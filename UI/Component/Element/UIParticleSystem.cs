﻿using UnityEngine;

namespace TMI.UI {

    public class UIParticleSystem : UIComponentWithId<ParticleSystem> {

        public void Play() {
            component.Play(true);
        }

        public void Stop() {
            component.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }

        public void StopAndClear() {
            component.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        }

    }

}