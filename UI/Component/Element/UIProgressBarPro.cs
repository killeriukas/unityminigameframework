﻿using TMI.UI.Extension;

namespace TMI.UI {

    public class UIProgressBarPro : UIComponentWithId<ProgressBarPro> {

        public float fillAmount {
			set {
				component.fillAmount = value;
			}
		}

		public string text {
			set { 
				component.text = value;
			}
		}

        public void SetAlternativeText(string id, string text) {
            component.SetAlternativeText(id, text);
        }

    }
}

