﻿using TMI.UI.Extension;
using System;

namespace TMI.UI {

    public class UITab : UIComponentWithId<Tab> {

        public event Action onTabOpened {
            add {
                component.onTabOpened += value;
            }
            remove {
                component.onTabOpened -= value;
            }
        }

        public event Action onTabClosed {
            add {
                component.onTabClosed += value;
            }
            remove {
                component.onTabClosed -= value;
            }
        }

    }

}