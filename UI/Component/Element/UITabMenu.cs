﻿using TMI.UI.Extension;
using System;

namespace TMI.UI {

    public class UITabMenu : UIComponentWithId<TabMenu> {

        public event Action onActiveTabClicked {
            add {
                component.onActiveTabClicked += value;
            }
            remove {
                component.onActiveTabClicked -= value;
            }
        }

    }

}