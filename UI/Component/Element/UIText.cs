﻿using UnityEngine;
using UnityEngine.UI;

namespace TMI.UI {

    [RequireComponent(typeof(Text))]
    public class UIText : UIComponentWithId<Text> {

		public string text {
			get { 
				return component.text;
			}
			set {
				component.text = value;
			}
		}

	}

}