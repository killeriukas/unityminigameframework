﻿using TMI.StringFormatManagement;
using TMI.UI.Extension;
using TMPro;
using UnityEngine;

namespace TMI.UI {

    [RequireComponent(typeof(TextMeshProUGUI))]
    public class UITextPro : UIComponentWithId<TextMeshProUGUI>, IInterpolatable {

        private INumberFormatter formatter;

        public IStringFormatPattern interpolatableStringFormatPattern = null;

        protected override void OnInitialize() {
            base.OnInitialize();
            formatter = GetComponent<BaseNumberFormatter>();
        }

        private string ApplyStringFormatter<TType>(TType value) {
            if(interpolatableStringFormatPattern == null) {
                return value.ToString();
            } else {
                return interpolatableStringFormatPattern.GenerateString(value);
            }
        }

        public string text {
			get { 
				return component.text;
			}
			set {
				component.text = value;
			}
		}

        public double interpolatableValue {
            set {
                if(formatter == null) {
                    text = ApplyStringFormatter(value);
                } else {
                    string formattedNumber = formatter.FormatNumber(value);
                    text = ApplyStringFormatter(formattedNumber);
                }
            }
        }

        public Color color {
            set {
                component.color = value; 
            }
        }
    }

}