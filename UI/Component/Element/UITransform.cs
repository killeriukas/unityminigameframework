﻿using System;
using UnityEngine.EventSystems;
using UnityEngine;

namespace TMI.UI {

	public class UITransform : UIComponentWithId<RectTransform> {

		public void SetPosition(Vector3 newPosition) {
		//	newPosition.z = component.localPosition.z;
			component.position = newPosition;
		}

	}

}