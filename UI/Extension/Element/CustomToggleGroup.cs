﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI.Extension {

    [DisallowMultipleComponent]
    /// <summary>
    /// A component that represents a group of UI.CustomToggles.
    /// </summary>
    /// <remarks>
    /// When using a group reference the group from a UI.CustomToggle. Only one member of a group can be active at a time.
    /// </remarks>
    public class CustomToggleGroup : UIBehaviour {
        [SerializeField] private bool m_AllowSwitchOff = false;

        /// <summary>
        /// Is it allowed that no toggle is switched on?
        /// </summary>
        /// <remarks>
        /// If this setting is enabled, pressing the toggle that is currently switched on will switch it off, so that no toggle is switched on. If this setting is disabled, pressing the toggle that is currently switched on will not change its state.
        /// Note that even if allowSwitchOff is false, the Toggle Group will not enforce its constraint right away if no toggles in the group are switched on when the scene is loaded or when the group is instantiated. It will only prevent the user from switching a toggle off.
        /// </remarks>
        public bool allowSwitchOff { get { return m_AllowSwitchOff; } set { m_AllowSwitchOff = value; } }

        private List<CustomToggle> m_Toggles = new List<CustomToggle>();

        protected CustomToggleGroup() { }

        private void ValidateToggleIsInGroup(CustomToggle toggle) {
            if(toggle == null || !m_Toggles.Contains(toggle))
                throw new ArgumentException(string.Format("CustomToggle {0} is not part of CustomToggleGroup {1}", new object[] { toggle, this }));
        }

        /// <summary>
        /// Notify the group that the given toggle is enabled.
        /// </summary>
        /// <param name="toggle">The toggle that got triggered on</param>
        public void NotifyToggleOn(CustomToggle toggle) {
            ValidateToggleIsInGroup(toggle);

            // disable all toggles in the group
            for(var i = 0; i < m_Toggles.Count; i++) {
                if(m_Toggles[i] == toggle)
                    continue;

                m_Toggles[i].isOnGroup = false;
            }
        }

        /// <summary>
        /// Unregister a toggle from the group.
        /// </summary>
        /// <param name="toggle">The toggle to remove.</param>
        public void UnregisterToggle(CustomToggle toggle) {
            if(m_Toggles.Contains(toggle))
                m_Toggles.Remove(toggle);
        }

        /// <summary>
        /// Register a toggle with the toggle group so it is watched for changes and notified if another toggle in the group changes.
        /// </summary>
        /// <param name="toggle">The toggle to register with the group.</param>
        public void RegisterToggle(CustomToggle toggle) {
            if(!m_Toggles.Contains(toggle))
                m_Toggles.Add(toggle);
        }

        /// <summary>
        /// Are any of the toggles on?
        /// </summary>
        /// <returns>Are and of the toggles on?</returns>
        public bool AnyTogglesOn() {
            return m_Toggles.Find(x => x.isOnGroup) != null;
        }

        /// <summary>
        /// Returns the toggles in this group that are active.
        /// </summary>
        /// <returns>The active toggles in the group.</returns>
        /// <remarks>
        /// Toggles belonging to this group but are not active either because their GameObject is inactive or because the Toggle component is disabled, are not returned as part of the list.
        /// </remarks>
        public IEnumerable<CustomToggle> ActiveToggles() {
            return m_Toggles.Where(x => x.isOnGroup);
        }

        /// <summary>
        /// Switch all toggles off.
        /// </summary>
        /// <remarks>
        /// This method can be used to switch all toggles off, regardless of whether the allowSwitchOff property is enabled or not.
        /// </remarks>
        public void SetAllTogglesOff() {
            bool oldAllowSwitchOff = m_AllowSwitchOff;
            m_AllowSwitchOff = true;

            for(var i = 0; i < m_Toggles.Count; i++)
                m_Toggles[i].isOnGroup = false;

            m_AllowSwitchOff = oldAllowSwitchOff;
        }
    }

}