﻿using UnityEngine;

namespace TMI.UI.Extension {

    public class UIEnabler : UIComponentWithId {

        public enum State {
            Off,
            On
        }

        [SerializeField]
        private State defaultState = State.Off;

        protected override void OnInitialize() {
            base.OnInitialize();
            gameObject.SetActive(defaultState == State.On);
        }

        public State state {
            set {
                if(defaultState != value) {
                    defaultState = value;
                    gameObject.SetActive(value == State.On);
                }
            }
        }

    }
        
}