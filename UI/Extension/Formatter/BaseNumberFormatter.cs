﻿
namespace TMI.UI.Extension {

    public abstract class BaseNumberFormatter : UIComponent, INumberFormatter {

        public abstract string FormatNumber(double number);

    }

}