﻿
using TMI.Helper;

namespace TMI.UI.Extension {

    public class BigNumberFormatter : BaseNumberFormatter {

        public override string FormatNumber(double number) {
            return FormattingHelper.FormatDouble(number);
        }

    }

}