﻿namespace TMI.UI.Extension {

    public interface INumberFormatter {
        string FormatNumber(double number);
    }

}