﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace TMI.UI.Extension {

	public class Grid : UIComponent {

        private enum State {
            Prestart = 0,
            Started
        }

        [SerializeField]
        private Horizontal cloneHorizontal;

        [SerializeField]
        private Cell cloneCell;

        private State currentState = State.Prestart;

        private Dictionary<int, Cell> cellsByInstanceId = new Dictionary<int, Cell>();
        private List<Horizontal> horizontalList = new List<Horizontal>();

        private LayoutElement cellLayoutElement;
        private RectTransform rectTransform;
        private int maxHorizontalCellAmount;
        private int startingChildrenCount;

        protected override void OnInitialize() {
            base.OnInitialize();

            rectTransform = GetComponent<RectTransform>();
            Assert.IsNull(cloneCell, "Clone cell in grid cannot be null! Name: " + name);

            cellLayoutElement = cloneCell.GetComponent<LayoutElement>();
            Assert.IsNull(cellLayoutElement, "Grid cell must contain LayoutElement with a preferred width set!");

            cloneCell.gameObject.SetActive(false);
            cloneCell.transform.SetParent(transform, false);

            cloneHorizontal.gameObject.SetActive(false);

            startingChildrenCount = transform.childCount;
        }

        protected override void Start() {
            base.Start();
            maxHorizontalCellAmount = Mathf.FloorToInt(rectTransform.rect.width / cellLayoutElement.preferredWidth);

            //TODO: rewrite this one into something meaningful
            int index = 0;
            Horizontal newHorizontal = CreateHorizontal();
            horizontalList.Add(newHorizontal);

            foreach(var kvp in cellsByInstanceId) {
                if(index == maxHorizontalCellAmount) {
                    newHorizontal = CreateHorizontal();
                    horizontalList.Add(newHorizontal);
                    index = 0;
                }

                newHorizontal.AddCell(kvp.Value);
                ++index;
            }
            //TODO: ends

            currentState = State.Started;
        }

        private Horizontal CreateHorizontal() {
            GameObject horizontalGo = GameObject.Instantiate(cloneHorizontal.gameObject, transform, false);
            horizontalGo.SetActive(true);

            Horizontal newHorizontal = horizontalGo.GetComponent<Horizontal>();
            return newHorizontal;
        }

        public Cell CloneCell() {
            //TODO: rethink this one a bit nicer
            Horizontal currentHorizontal = null;

            switch(currentState) {
                case State.Prestart:
                    break;
                case State.Started:
                    if(horizontalList.Count > 0) {
                        currentHorizontal = horizontalList[horizontalList.Count - 1];

                        if(currentHorizontal.Count == maxHorizontalCellAmount) {
                            currentHorizontal = CreateHorizontal();
                            horizontalList.Add(currentHorizontal);
                        }
                    } else {
                        currentHorizontal = CreateHorizontal();
                        horizontalList.Add(currentHorizontal);
                    }
                    break;
            }

            GameObject newCellGo = GameObject.Instantiate(cloneCell.gameObject, transform, false);

            //initialize all the TOP components of the cell game object including the cell itself
            IUIComponent[] topUIComponents = newCellGo.GetComponents<IUIComponent>();
            foreach(IUIComponent uiComponent in topUIComponents) {
                uiComponent.OnCloneComplete();
                uiComponent.Setup(initializer);
            }

            Cell newCell = newCellGo.GetComponent<Cell>();
            newCellGo.SetActive(true);

            int instanceId = newCell.GetInstanceID();
            cellsByInstanceId.Add(instanceId, newCell);


            currentHorizontal?.AddCell(newCell);
            //TODO: ends

            return newCell;
        }

        public int GetCellHorizontalIndex(Cell cell) {
            for(int i = 0; i < horizontalList.Count; ++i) {
                if(horizontalList[i].Contains(cell)) {
                    return startingChildrenCount + i;
                }
            }
            throw new ArgumentException("GetCellHorizontalIndex() -> No cell found in the horizontal list!");
        }

        private int FindHorizontalIndexByCell(Cell cell) {
            for(int i = 0; i < horizontalList.Count; ++i) {
                if(horizontalList[i].Contains(cell)) {
                    return i;
                }
            }
            throw new ArgumentException("FindHorizontalByCell() -> No cell found in the horizontal list!");
        }

        public void DestroyCell(Cell cell) {
            //TODO: re-think this. Might find a nicer solution

            //remove cell from the horizontal list
            int horizontalIndex = FindHorizontalIndexByCell(cell);
            Horizontal currentHorizontal = horizontalList[horizontalIndex];
            currentHorizontal.RemoveCell(cell);

            //destroy the cell
            int instanceId = cell.GetInstanceID();
            bool wasRemoved = cellsByInstanceId.Remove(instanceId);
            if(wasRemoved) {
                GameObject.Destroy(cell.gameObject);
            } else {
                throw new ArgumentException("Grid cell was not created through this list. InstanceId: " + instanceId + " Name: " + cell.name);
            }

            //traverse the horizontal list and move all the cells back up one by one
            if(currentHorizontal.Count > 0) {

                int nextHorizontalIndex = horizontalIndex + 1;

                while(nextHorizontalIndex < horizontalList.Count) {
                    Horizontal nextHorizontal = horizontalList[nextHorizontalIndex];

                    //if next horizontal has any cells in it
                    if(nextHorizontal.Count > 0) {
                        //take it from the next one
                        Cell receivedCell = nextHorizontal.GetCell(0);
                        nextHorizontal.RemoveCell(receivedCell);

                        //put it to the previous one
                        currentHorizontal.AddCell(receivedCell);

                        currentHorizontal = nextHorizontal;
                    }

                    ++nextHorizontalIndex;
                }

                //if the last horizontal line became empty - delete it
                Horizontal lastHorizontal = horizontalList[horizontalList.Count - 1];
                if(0 == lastHorizontal.Count) {
                    horizontalList.Remove(lastHorizontal);
                    GameObject.Destroy(lastHorizontal.gameObject);
                }

            } else {
                //destroy the horizontal line if it's empty
                horizontalList.Remove(currentHorizontal);
                GameObject.Destroy(currentHorizontal.gameObject);
            }

            //TODO: ends
        }

    }

}