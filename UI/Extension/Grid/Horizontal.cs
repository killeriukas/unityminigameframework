﻿using System.Collections.Generic;

namespace TMI.UI.Extension {

    public class Horizontal : UIComponent {

        private List<Cell> cells = new List<Cell>();

        public int Count => cells.Count;

        public Cell GetCell(int index) {
            return cells[index];
        }

        public void AddCell(Cell cell) {
            cell.transform.SetParent(transform, false);
            cells.Add(cell);
        }

        public void RemoveCell(Cell cell) {
            cells.Remove(cell);
        }

        public bool Contains(Cell cell) {
            return cells.Contains(cell);
        }


    }

}