﻿
namespace TMI.UI.Extension {

    public interface IInterpolatable {
        double interpolatableValue { set; }
    }

}