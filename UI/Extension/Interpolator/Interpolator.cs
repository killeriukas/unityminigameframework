﻿using System;
using TMI.Core;
using TMI.Mathematics;
using UnityEngine;

namespace TMI.UI.Extension {

    public class Interpolator : UIComponent, IUpdatable {

        private enum InterpolationState {
            Up,
            Down,
            UpAndDown,
            None
        }

        [SerializeField]
        [Range(50, 1000)]
        private double interpolationSpeedInMS = 200;

        [SerializeField]
        private InterpolationState interpolationState = InterpolationState.UpAndDown;

        private IInterpolatable uiInterpolatable;

        private EaseDouble ease;
        private bool isSet = false;

        private IExecutionManager executionManager;

        protected override void OnInitialize() {
            base.OnInitialize();
            uiInterpolatable = GetComponent<IInterpolatable>();
            Assert.IsNull(uiInterpolatable, "Interpolator cannot work on an object without IUIInterpolatable component on it!");
        }

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            executionManager = initializer.GetManager<ExecutionManager, IExecutionManager>();
        }

        private double startNumber;
        private double currentNumber;
        private double endNumber;

        public double number {
            set {
                if(isSet) {
                    bool isGoingUp = currentNumber < value;

                    switch(interpolationState) {
                        case InterpolationState.Up:
                            if(isGoingUp) {
                                Interpolate(value);
                            } else {
                                SetInstantly(value);
                            }
                            break;
                        case InterpolationState.Down:
                            if(isGoingUp) {
                                SetInstantly(value);
                            } else {
                                Interpolate(value);
                            }
                            break;
                        case InterpolationState.UpAndDown:
                            Interpolate(value);
                            break;
                        case InterpolationState.None:
                            SetInstantly(value);
                            break;
                    }

                } else {
                    isSet = true;
                    SetInstantly(value);
                }
            }
        }

        private void SetInstantly(double value) {
            startNumber = currentNumber = endNumber = value;
            uiInterpolatable.interpolatableValue = currentNumber;
        }

        private void Interpolate(double value) {
            if(ease == null) {
                startNumber = endNumber;
                endNumber = value;
                executionManager.Register(this, OnUpdateNumber);
            } else {
                startNumber = currentNumber;
                endNumber = value;
            }
            LinearInterpolation interpolation = new LinearInterpolation();
            InterpolatorDouble interpolator = new InterpolatorDouble(startNumber, endNumber, interpolation);
            ease = new EaseDouble(interpolator, TimeSpan.FromMilliseconds(interpolationSpeedInMS), OnInterpolationFinished);
        }

        private void OnInterpolationFinished() {
            ease = null;
            executionManager.Unregister(this);
        }

        private ExecutionManager.Result OnUpdateNumber() {
            TimeSpan deltaTimeSpan = TimeSpan.FromSeconds(Time.deltaTime);
            currentNumber = ease.Update(deltaTimeSpan);
            uiInterpolatable.interpolatableValue = currentNumber;
            return ExecutionManager.Result.Continue;
        }

        protected override void OnDestroy() {
            if(ease != null) {
                ease = null;
                executionManager.Unregister(this);
            }
            
            base.OnDestroy();
        }

    }

}