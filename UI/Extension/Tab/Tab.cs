﻿using System;
using TMI.Helper;
using UnityEngine;

namespace TMI.UI.Extension {

    public class Tab : UIComponent {

        [SerializeField]
        private UIToggle toggle;

        public event Action onTabOpened;
        public event Action onTabClosed;

        public event Action<Tab, bool> onValueChanged;

        protected override void OnInitialize() {
            base.OnInitialize();
            toggle.onValueChanged += OnToggleValueChanged;
        }

        private void OnToggleValueChanged(bool currentValue) {
            onValueChanged?.Invoke(this, currentValue);
        }

        public void Activate(bool activate) {
            gameObject.SetActive(activate);

            if(activate) {
                onTabOpened?.Invoke();
            } else {
                onTabClosed?.Invoke();
            }
        }

        public void ActivateToggle() {
            toggle.isOn = true;
            gameObject.SetActive(true);

            onTabOpened?.Invoke();
        }

        protected override void OnDestroy() {
            toggle.onValueChanged -= OnToggleValueChanged;
            base.OnDestroy();
        }

    }

}