﻿using System;
using UnityEngine;

namespace TMI.UI.Extension {

    public class TabMenu : UIComponent {

        private Tab[] allTabs;

        [SerializeField]
        private Tab activeTab;

        public event Action onActiveTabClicked;

        protected override void OnInitialize() {
            base.OnInitialize();
            allTabs = GetComponentsInChildren<Tab>(true);
        }

        protected override void Start() {
            base.Start();

            UIToggle[] toggles = GetComponentsInChildren<UIToggle>(true);

            foreach(UIToggle toggle in toggles) {
                toggle.isOn = false;
            }

            foreach(Tab tab in allTabs) {
                tab.Activate(false);
            }

            activeTab.ActivateToggle();

            foreach(Tab tab in allTabs) {
                tab.onValueChanged += OnTabChanged;
            }
        }

        private void OnTabChanged(Tab newTab, bool currentValue) {
            if(currentValue) {
                bool sameTab = activeTab == newTab;
                if(sameTab) {
                    onActiveTabClicked?.Invoke();
                } else {
                    activeTab = newTab;
                    activeTab.Activate(true);
                }
            } else {
                newTab.Activate(false);
            }
        }

        protected override void OnDestroy() {
            foreach(Tab tab in allTabs) {
                tab.onValueChanged -= OnTabChanged;
            }
            base.OnDestroy();
        }

    }

}
