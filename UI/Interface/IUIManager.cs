﻿using TMI.AssetManagement;
using TMI.Core;

namespace TMI.UI {

    public interface IUIManager : IManager {
        TUIController LoadUI<TUIController>(bool refresh = true) where TUIController : BaseUIController;
        void PrecacheUI(ISceneGroupComplete sceneGroup);
        void UnloadUIScene(string sceneName);
        void HideAllUI();
        int CreateSortingOrder(string sortingLayerName, int relativeDifference = 0);
    }

}