﻿using TMI.Notification;

namespace TMI.UI {

    public class UISceneHiddenNotification : BasePredicateNotification {

        public UISceneHiddenNotification(string sceneName) : base(sceneName) {
        }

    }
}