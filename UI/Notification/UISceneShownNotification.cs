﻿using TMI.Notification;

namespace TMI.UI {

    public class UISceneShownNotification : BasePredicateNotification {

        public UISceneShownNotification(string sceneName) : base(sceneName) {
        }

    }
}