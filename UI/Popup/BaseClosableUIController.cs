﻿using System;
using TMI.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI.Popup {

    public abstract class BaseClosableUIController : BaseUIController {

        [SerializeField]
        private UIButton closeButton;

        public bool enableCloseButton {
            set {
                closeButton?.gameObject.SetActive(value);
            }
        }

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);

            //TODO: may need to rethink the popup inheritance
            if(closeButton != null) {
                closeButton.onButtonClick += Close;
            }
        }

        public override void Refresh() {
            base.Refresh();
            enableCloseButton = true;
        }

        public event Action<PointerEventData> onCloseClicked {
            add {
                closeButton.onButtonClick += value;
            }
            remove {
                closeButton.onButtonClick -= value;
            }
        }

        public void ClearCloseOnButtonClick() {
            if(closeButton != null) {
                closeButton.ClearOnButtonClick();
                closeButton.onButtonClick += Close;
            }
        }

        protected override void OnDestroy() {

            //TODO: may need to rethink the popup inheritance
            if(closeButton != null) {
                closeButton.onButtonClick -= Close;
            }

            base.OnDestroy();
        }

    }

}