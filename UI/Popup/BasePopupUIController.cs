﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI.Popup {

    public abstract class BasePopupUIController : BaseClosableUIController {

        [SerializeField]
        private UITextPro popupNameText;

        [SerializeField]
        private UITextPro popupDescriptionText;

        [SerializeField]
        private UIButton popupDismissButton;

        [SerializeField]
        private UITextPro dismissButtonText;

        public string popupName {
            set {
                popupNameText.text = value;
            }
        }

        public string popupDescription {
            set {
                popupDescriptionText.text = value;
            }
        }

        public string dismissButtonName {
            set {
                dismissButtonText.text = value;
            }
        }

        public event Action<PointerEventData> onDismissButtonClick {
            add {
                popupDismissButton.onButtonClick += value;
            }
            remove {
                popupDismissButton.onButtonClick -= value;
            }
        }

        public void ClearDismissOnButtonClick() {
            popupDismissButton.ClearOnButtonClick();
        }

        public bool isDismissButtonInteractable {
            get {
                return popupDismissButton.interactable;
            }
            set {
                popupDismissButton.interactable = value;
            }
        }

    }

}