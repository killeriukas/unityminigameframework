﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI.Popup {

    public class ConfirmationPopupUIController : InformationPopupUIController {

        [SerializeField]
        private UIButton confirmButton;

        [SerializeField]
        private UITextPro confirmButtonText;

        public event Action<PointerEventData> onConfirmButtonClick {
            add {
                confirmButton.onButtonClick += value;
            }
            remove {
                confirmButton.onButtonClick -= value;
            }
        }

        public string confirmButtonName {
            set {
                confirmButtonText.text = value;
            }
        }

        public override void Hide() {
            confirmButton.ClearOnButtonClick();
            base.Hide();
        }

    }


}