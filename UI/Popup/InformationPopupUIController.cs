﻿using TMI.Core;

namespace TMI.UI.Popup {

    public class InformationPopupUIController : BasePopupUIController {

        public override void Setup(IInitializer initializer) {
            base.Setup(initializer);
            onDismissButtonClick += Close;
        }

        protected override void OnDestroy() {
            onDismissButtonClick -= Close;
            base.OnDestroy();
        }

    }


}