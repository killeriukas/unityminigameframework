﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMI.UI.Popup {

    public class TwoOptionPopupUIController : BasePopupUIController {

        [SerializeField]
        private UIButton optionTwoButton;

        [SerializeField]
        private UITextPro optionTwoText;

        public event Action<PointerEventData> onOptionTwoButtonClick {
            add {
                optionTwoButton.onButtonClick += value;
            }
            remove {
                optionTwoButton.onButtonClick -= value;
            }
        }

        public string optionTwoButtonName {
            set {
                optionTwoText.text = value;
            }
        }

    }


}