﻿using TMI.Core;
using TMI.AssetManagement;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMI.LogManagement;
using TMI.Helper;

namespace TMI.UI {

    public class UIManager : BaseManager, IUIManager {

        private enum State {
            Running,
            Precaching,
            Unknown
        }

        private State state = State.Unknown;

        private class UIMapping {
            public Scene scene;
            public BaseUIController controller;
        }

        private readonly Dictionary<string, int> sortOrderNumberByLayerName = new Dictionary<string, int>();

		private readonly Dictionary<Type, UIMapping> cachedUIByType = new Dictionary<Type, UIMapping>();
        private readonly Dictionary<string, Type> cachedUiTypeByName = new Dictionary<string, Type>();

        protected override void Awake() {
            base.Awake();

            foreach(SortingLayer sortingLayer in SortingLayer.layers) {
                sortOrderNumberByLayerName.Add(sortingLayer.name, 0);
            }
        }

        public TUIController LoadUI<TUIController>(bool refresh = true) where TUIController : BaseUIController {
            Assert.IsFalse<InvalidOperationException>(state == State.Running, "Cannot load the UI whilst UIManager is not running! Try OnUIReady() instead. Current state: " + state);

			Type type = typeof(TUIController);
            UIMapping mapping = cachedUIByType[type];
            BaseUIController controller = mapping.controller;

            if(refresh) {
                controller.Refresh();
            }

			return (TUIController)controller;
		}

        private void DestroyAllLoadedScenes() {
            foreach(var kvp in cachedUIByType) {
                UIMapping mapping = kvp.Value;
                SceneManager.UnloadSceneAsync(mapping.scene);
            }
            cachedUIByType.Clear();
            cachedUiTypeByName.Clear();
        }

        public void PrecacheUI(ISceneGroupComplete sceneGroup) {
            state = State.Precaching;

            //clear sorting order numbers
            foreach(SortingLayer sortingLayer in SortingLayer.layers) {
                sortOrderNumberByLayerName[sortingLayer.name] = 0;
            }

            IEnumerable<Scene> uiScenes = sceneGroup.GetAll();
			foreach(Scene scene in uiScenes) {
                PrecacheUIScene(scene);

            }

            state = State.Running;

            foreach(var kvp in cachedUIByType) {
                BaseUIController baseUIController = kvp.Value.controller;
                baseUIController.OnUIReady();
            }
        }

        private void PrecacheUIScene(Scene scene) {
            GameObject[] rootGameObjects = scene.GetRootGameObjects();
            HierarchyHelper.FindAndDeleteEventSystem(rootGameObjects);
            HierarchyHelper.FindAndDeleteCamera(rootGameObjects);

            foreach(GameObject sceneGo in rootGameObjects) {
                BaseUIController baseUIController = sceneGo.GetComponent<BaseUIController>();
                if(baseUIController != null) {
                    baseUIController.Setup(initializer);
                    baseUIController.Hide();

                    Type controllerType = baseUIController.GetType();
                    UIMapping uiMapping = CreateUIMapping(scene, baseUIController);
                    Logging.Log(this, "Loading UI controller for [{0}].", controllerType.Name);

                    cachedUIByType.Add(controllerType, uiMapping);
                    cachedUiTypeByName.Add(scene.name, controllerType);
                    break;
                }
            }
        }

        private static UIMapping CreateUIMapping(Scene scene, BaseUIController baseUIController) {
            UIMapping uiMapping = new UIMapping();
            uiMapping.scene = scene;
            uiMapping.controller = baseUIController;
            return uiMapping;
        }

        public void UnloadUIScene(string sceneName) {
            Type uiType = cachedUiTypeByName[sceneName];
            UIMapping uiMapping = cachedUIByType[uiType];
            CloseAndDestroy(uiMapping.controller);
            cachedUiTypeByName.Remove(sceneName);

            Logging.Log(this, "Unloading UI controller for [{0}] in scene [{1}].", uiType.Name, sceneName);
        }

        public void HideAllUI() {
            foreach(var kvp in cachedUIByType) {
                kvp.Value.controller.Hide();
            }
        }

        private void CloseAndDestroy(BaseUIController controller) {
			controller.Hide();
            Type type = controller.GetType();

            UIMapping mapping = cachedUIByType[type];
            cachedUIByType.Remove(type);

            SceneManager.UnloadSceneAsync(mapping.scene);
        }

        protected override void OnDestroy() {
            DestroyAllLoadedScenes();
            state = State.Unknown;
            base.OnDestroy();
        }

        public int CreateSortingOrder(string sortingLayerName, int relativeDifference = 0) {
            Assert.IsTrue<ArgumentException>(relativeDifference < 0, "Relative difference cannot be < 0! Sorting layer: " + sortingLayerName + " Diff: " + relativeDifference);

            int currentSortingOrder = sortOrderNumberByLayerName[sortingLayerName];

            if(0 == relativeDifference) {
                ++currentSortingOrder;
            } else {
                currentSortingOrder += relativeDifference;
            }

            sortOrderNumberByLayerName[sortingLayerName] = currentSortingOrder;
            return currentSortingOrder;
        }
    }


}