﻿using System;
using TMI.Pattern;
using UnityEngine;

namespace TMI.VfxManagement {

    public interface IVfx : IPoolable {
        void Play();
        void Stop();
        void SetPosition(Vector3 position);
        void AttachTo(Transform attachTransform);
        event Action<IVfx> onVfxEnded;
    }

}