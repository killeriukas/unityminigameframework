﻿using TMI.AssetManagement;
using TMI.Core;
using TMI.Helper;
using UnityEngine;

namespace TMI.VfxManagement {

    public class VfxManager : BaseNotificationManager, IAssetGroupInitialization {

        private IAssetGroupComplete assetGroup;

        //TODO: store pooled vfx here

        public IVfx CreateEffect(string effectPrefabId, Transform attachTo) {
            VisualEffectAccessor vfx = CreateVfx(effectPrefabId);
            vfx.transform.SetParent(attachTo, false);
            return vfx;
        }

        public IVfx CreateEffect(string effectPrefabId) {
            VisualEffectAccessor vfx = CreateVfx(effectPrefabId);
            return vfx;
        }

        private VisualEffectAccessor CreateVfx(string effectPrefabId) {
            VisualEffectAccessor effectPrefab = assetGroup.GetAsset<VisualEffectAccessor>(effectPrefabId);
            VisualEffectAccessor vfx = HierarchyHelper.Instantiate(effectPrefab);
            vfx.Initialize();
            vfx.onVfxEnded += OnVfxFinished;
            return vfx;
        }

        private void OnVfxFinished(IVfx vfx) {
            vfx.onVfxEnded -= OnVfxFinished;
            ReturnEffect(vfx);
        }

        public void ReturnEffect(IVfx vfxEffect) {
            vfxEffect.Uninitialize();
            //TODO: later add this back to the list of poolable items
        }


        //initialize vfx here (or grab from the pool) and return vfx handle to be attached to anything you want

        //there is only one vfx and itcan be attached to anything / moved anywhere

        //once the vfx runs out, return it back to the vfx manager

        //store all loaded vfx here
        public void InitializeAssets(IAssetGroupComplete assetGroup) {
            this.assetGroup = assetGroup;

            //precache all vfx into the pool instead of storing asset group
        }
    }

}