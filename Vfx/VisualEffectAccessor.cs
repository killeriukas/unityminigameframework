﻿using System;
using TMI.Helper;
using TMI.ParticleManagement;
using UnityEngine;

namespace TMI.VfxManagement {

    public class VisualEffectAccessor : MonoBehaviour, IVfx {

        [SerializeField]
        private ParticleSystem vfxParticleSystem;

        private ParticleSystemEndCallback particleSystemEndCallback;

        public event Action<IVfx> onVfxEnded;

        private void Awake() {
            particleSystemEndCallback = GetComponentInChildren<ParticleSystemEndCallback>();

            if(particleSystemEndCallback != null) {
                particleSystemEndCallback.onParticleSystemStopped += ParticleSystemEnded;
            }
        }

        private void ParticleSystemEnded() {
            GeneralHelper.CallNullCheck(onVfxEnded, this);
        }

        public void Initialize() {
            
        }

        public void Play() {
            vfxParticleSystem.Play(true);
        }

        public void SetPosition(Vector3 position) {
            transform.position = position;
        }

        public void AttachTo(Transform attachTransform) {
            transform.SetParent(attachTransform, false);
        }

        public void Stop() {
            vfxParticleSystem.Stop(true);
        }

        public void Uninitialize() {
            Stop();
            onVfxEnded = null;

            //TODO: this should not destroy itself when we start using the object pool system
            GameObject.Destroy(gameObject);
        }

        private void OnDestroy() {
            if(particleSystemEndCallback != null) {
                particleSystemEndCallback.onParticleSystemStopped -= ParticleSystemEnded;
                particleSystemEndCallback = null;
            }

            onVfxEnded = null;
        }
    }


}