﻿using System;

//re-think this later on
#if USE_NEWTONSOFT_JSON
using Newtonsoft.Json;
#endif

namespace TMI.Number {

    //re-think this later on
    #if USE_NEWTONSOFT_JSON
    [JsonObject(MemberSerialization.OptIn)]
    #endif
    public struct Currency {

        //re-think this later on
        #if USE_NEWTONSOFT_JSON
        [JsonProperty]
        #endif
        public readonly string type;

        //re-think this later on
        #if USE_NEWTONSOFT_JSON
        [JsonProperty]
        #endif
        public readonly double amount;

        public Currency(string type, double amount) {
            this.type = type;
            this.amount = amount;
        }

        public static Currency Zero(string type) {
            return new Currency(type, 0);
        }

        public static Currency operator+(Currency c1, Currency c2) {
            Assert.IsFalse<ArgumentException>(c1.type.Equals(c2.type), "Can't add two different currencies! " + c1.type + " != " + c2.type);

            double newAmount = c1.amount + c2.amount;
            Currency newCurrency = new Currency(c1.type, newAmount);
            return newCurrency;
        }

        public static Currency operator-(Currency c1, Currency c2) {
            Assert.IsFalse<ArgumentException>(c1.type.Equals(c2.type), "Can't add two different currencies! " + c1.type + " != " + c2.type);

            double newAmount = c1.amount - c2.amount;
            Assert.IsTrue<ArgumentException>(newAmount < 0, "Currency cannot be negative! c1: " + c1.ToString() + " c2: " + c2.ToString());

            Currency newCurrency = new Currency(c1.type, newAmount);
            return newCurrency;
        }

        public static bool operator>=(Currency c1, Currency c2) {
            Assert.IsFalse<ArgumentException>(c1.type.Equals(c2.type), "Can't compare different currencies! " + c1.type + " != " + c2.type);
            
            return c1.amount >= c2.amount;
        }

        public static bool operator<=(Currency c1, Currency c2) {
            Assert.IsFalse<ArgumentException>(c1.type.Equals(c2.type), "Can't compare different currencies! " + c1.type + " != " + c2.type);

            return c1.amount <= c2.amount;
        }

        public static bool operator >(Currency c1, Currency c2) {
            Assert.IsFalse<ArgumentException>(c1.type.Equals(c2.type), "Can't compare different currencies! " + c1.type + " != " + c2.type);

            return c1.amount > c2.amount;
        }

        public static bool operator <(Currency c1, Currency c2) {
            Assert.IsFalse<ArgumentException>(c1.type.Equals(c2.type), "Can't compare different currencies! " + c1.type + " != " + c2.type);

            return c1.amount < c2.amount;
        }

        public static Currency operator*(Currency currency, double multiplier) {
            Assert.IsTrue<ArgumentException>(multiplier < 0, "Cannot multiply currency with a negative multiplier: " + multiplier);

            double total = currency.amount * multiplier;
            Currency finalCurrency = new Currency(currency.type, total);
            return finalCurrency;
        }

        public static Currency operator *(double multiplier, Currency currency) {
            return currency * multiplier;
        }

        public override string ToString() {
            return type + ":" + amount;
        }
    }
}
