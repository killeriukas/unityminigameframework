﻿using System.Collections.Generic;
using TMI.Number;

public interface IWallet {
    bool CanAfford(Currency cost);
    bool CanAfford(Dictionary<string, Currency> cost);
}
