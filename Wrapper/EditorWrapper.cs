﻿
namespace TMI.Wrapper {

    public abstract class EditorWrapper<TClass> {

        protected readonly TClass _class;

        protected EditorWrapper(TClass _class) {
            this._class = _class;
        }
    }

}